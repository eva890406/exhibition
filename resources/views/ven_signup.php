<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>覽得看 - 主辦方註冊</title>
    <link rel="icon" type="image/x-icon" href="assets/img/icon.jpg" />
    <!-- Font Awesome icons (free version)-->
    <script src="https://use.fontawesome.com/releases/v5.12.1/js/all.js" crossorigin="anonymous"></script>
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic" rel="stylesheet"
        type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css" />
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="css/styles.css" rel="stylesheet" />
    <link href="css/page.css" rel="stylesheet" />
</head>

<body id="page-top">
    <!-- Navigation-->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
        <div class="container">
            <a class="navbar-brand js-scroll-trigger" href="index.php"><i class='fas fa-camera-retro'></i>
                <font face="微軟正黑體">_覽得看</font>
            </a><button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">Menu<i class="fas fa-bars ml-1"></i></button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav text-uppercase ml-auto">
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="login_index.php">
                            <font face="微軟正黑體">登入</font>
                        </a></li>
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="category.php">
                            <font face="微軟正黑體">展覽類別</font>
                        </a></li>
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="">
                            <font face="微軟正黑體">願望清單</font>
                        </a></li>
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="">
                            <font face="微軟正黑體">購物車</font>
                        </a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- Masthead-->
    <header class="masthead3">
    </header>
    <!-- Services-->
    <div class="text-center">
        <h2 class="section-heading text-uppercase ">
            <i class='fas fa-city' style='font-size:48px' color=#a3d1d1></i>
            <span class="section-heading-text">主辦方註冊</span>
        </h2>
        <h3 class="section-subheading text-muted">
            <font color="#ff9797">Register as an Organizer</font>
        </h3>
    </div>
    <div class="sign-up--font">
        <center>
            <font>&nbsp;</font>
            <form action="/signupVendorsMember" method="post">
            <input type="text" name="id" style="display:none; margin:30px">
            <div style=" margin:30px;"><?= $msg ?? 'HELLO'?></div>
            <form action="/signupVendorsMember" method="post">
                主辦方名稱:&nbsp;<input type="text" class="sign-up-form--select" name="name"><br><br>
                &emsp;&emsp;&emsp;
                
                帳號:&nbsp;<input type="text" class="sign-up-form--select" name="account"><br><br>
                &emsp;&emsp;&emsp;
                
                密碼:&nbsp;<input type="password" class="sign-up-form--select" name="password"><br><br>
                &emsp;&emsp;&emsp;&emsp;&emsp;

                
                <textarea style="font-family:微軟正黑體; color:#00aaaa" name="intro">*請輸入主辦方簡介</textarea><br><br>
                &emsp;&emsp;&emsp;&emsp;&emsp;

                <textarea style="font-family:微軟正黑體; color:#00aaaa" name="contact">*請輸入聯絡方式(聯絡人/電話/E-mail等)</textarea><br><br>

                <input class="sign-up-form--submit-button" type="submit" value="註冊">
                <br><br>
            </form>
        </center>
    </div>
    <!-- Bootstrap core JS-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
    <!-- Third party plugin JS-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
    <!-- Contact form JS-->
    <script src="assets/mail/jqBootstrapValidation.js"></script>
    <script src="assets/mail/contact_me.js"></script>
    <!-- Core theme JS-->
    <script src="js/scripts.js"></script>
</body>

</html>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>覽得看 - 埃及文化展</title>
    <link rel="icon" type="image/x-icon" href="assets/img/icon.jpg" />
    <!-- Font Awesome icons (free version)-->
    <script src="https://use.fontawesome.com/releases/v5.12.1/js/all.js" crossorigin="anonymous"></script>
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic" rel="stylesheet"
        type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css" />
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="css/styles.css?v=" rel="stylesheet" />
    <link href="css/page.css" rel="stylesheet" />
</head>

<body id="page-top">
    <!-- Navigation-->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
        <div class="container">
            <a class="navbar-brand js-scroll-trigger" href="index.php"><i class='fas fa-camera-retro'></i>
                <font face="微軟正黑體">_覽得看</font>
            </a><button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">Menu<i class="fas fa-bars ml-1"></i></button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav text-uppercase ml-auto">
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="">
                            <font face="微軟正黑體">登入</font>
                        </a></li>
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#category.php">
                            <font face="微軟正黑體">展覽類別</font>
                        </a></li>
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="">
                            <font face="微軟正黑體">願望清單</font>
                        </a></li>
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="">
                            <font face="微軟正黑體">購物車</font>
                        </a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- Masthead-->
    <header class="masthead3">
    </header>
    <!-- Services-->
    <section class="page-section" id="services">
        <div class="container">
            <div class="text-center">
                <h2 class="section-heading text-uppercase ">
                    <i class='fas fa-eye' style='font-size:48px' color=#a3d1d1></i>
                    <font face="微軟正黑體" color="a3d1d1">埃及法老展</font>
                </h2>
                <h3 class="section-subheading text-muted">
                    <font color="#ff9797">Egyptian Culture</font>
                </h3>
                <img src="assets/img/展覽圖/埃及展.jpg" alt="圖片無法顯示" width="550px">
            </div>
            <center>
                <table border-style="none">
                    　<tr>
                        　<td>展覽名稱:</td>
                        　<td>埃及法老展</td>
                    </tr>
                    <tr>
                        　<td>展覽時間:</td>
                        　<td>2020/6/1~7/1</td>
                    </tr>
                    <tr>
                        　<td>展覽地點:</td>
                        　<td>國立故宮博物院</td>
                    </tr>
                    <tr>
                        　<td>票價:</td>
                        　<td>全票 360 <br>半票 180</td>
                    </tr>
                </table>
            </center>
        </div>
    </section>
    <!-- Portfolio Grid-->
    <section class="page-section bg-light" id="portfolio">
        <center>

            <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger portfolio-link" data-toggle="modal"
                data-target="#portfolioModal">
                <font face="微軟正黑體" color="white">&nbsp;&nbsp;加入購物車&nbsp;&nbsp;</font>
            </a>
            <a class="btn btn-primary2 btn-xl text-uppercase js-scroll-trigger portfolio-link" data-toggle="modal" 
            data-target="#portfolioModal2">
                <font face="微軟正黑體" color="white">加入願望清單</font>
            </a>
        </center>
        <center>
            <a class="btn btn-primary2 btn-xl text-uppercase js-scroll-trigger" href="ex_modify.php">
                <font face="微軟正黑體" color="#fff">修改展覽</font>
            </a>
            <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger">
                <font face="微軟正黑體" color="white">刪除展覽</font>
            </a>
        </center>
    </section>
    

    <!-- Modal 1-->
    <div class="portfolio-modal modal fade" id="portfolioModal" tabindex="-1" role="dialog" aria-hidden="true" style="width:100%; margin:auto">
        <div class="modal-dialog" style="margin:auto;">
            <div class="modal-content" style="max-width:600px;margin:auto;margin-top:100px;">
                <h4 class="section-heading text-uppercase ">
                    <i class='fas fa-money-check' style='font-size:36px;color:#a3d1d1'></i>
                    <font face="微軟正黑體" color="a3d1d1">確認購買張數</font>
                </h4>
                <font face="微軟正黑體" color="#ff9797">票卷張數:
                    <input type="number" class="sign-up-form--select" name="tickets" min="1" max="10" value="1">
                </font>
                <center>
                    <a class="btn btn-primary2 btn-xl text-uppercase js-scroll-trigger" style="width:150px;margin:20px"
                        href="">
                        <font face="微軟正黑體" color="#fff">確認</font>
                    </a>
                </center>
            </div>
        </div>
    </div>

    <!-- Modal 2-->
    <div class="portfolio-modal modal fade" id="portfolioModal2" tabindex="-1" role="dialog" aria-hidden="true" style="width:100%; margin:auto">
        <div class="modal-dialog" style="margin:auto;">
            <div class="modal-content" style="max-width:600px;margin:auto;margin-top:100px;">
                <h4 class="section-heading text-uppercase ">
                    <i class='fas fa-gift' style='font-size:36px;color:#ff9797'></i>
                    <font face="微軟正黑體" color="#ff9797">已加入願望清單</font>
                </h4>
                <center>
                    <a class="btn btn-primary2 btn-xl text-uppercase js-scroll-trigger" style="width:150px;margin:20px"
                        href="">
                        <font face="微軟正黑體" color="#fff">確認</font>
                    </a>
                </center>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JS-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
    <!-- Third party plugin JS-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
    <!-- Contact form JS-->
    <script src="assets/mail/jqBootstrapValidation.js"></script>
    <script src="assets/mail/contact_me.js"></script>
    <!-- Core theme JS-->
    <script src="js/scripts.js"></script>
</body>

</html>
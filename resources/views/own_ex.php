<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>覽得看 - 主辦展覽</title>
    <link rel="icon" type="image/x-icon" href="assets/img/icon.jpg" />
    <!-- Font Awesome icons (free version)-->
    <script src="https://use.fontawesome.com/releases/v5.12.1/js/all.js" crossorigin="anonymous"></script>
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic" rel="stylesheet"
        type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css" />
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="css/styles.css" rel="stylesheet" />
    <link href="css/page.css" rel="stylesheet" />
</head>

<body id="page-top">
    <!-- Navigation-->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
        <div class="container">
            <a class="navbar-brand js-scroll-trigger" href="index.php"><i class='fas fa-camera-retro'></i>
                <font face="微軟正黑體">_覽得看</font>
            </a><button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">Menu<i class="fas fa-bars ml-1"></i></button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav text-uppercase ml-auto">
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="">
                            <font face="微軟正黑體">登出</font>
                        </a></li>
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="category.php">
                            <font face="微軟正黑體">展覽類別</font>
                        </a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- Masthead-->
    <header class="masthead2">
        <div class="container">
            <div class="masthead-heading text-uppercase" align=left>
                <font color="#00aaaa">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&emsp;&emsp;&emsp;Exhibition
                </font>
                <font color=#ff9797>&nbsp;Table</font>
            </div>
            <div class="masthead-heading text-uppercase">
                <font color="#ffffff" face="微軟正黑體">主辦展覽一覽表</font>
            </div>
        </div>
    </header>
    <!-- Services-->
    <section class="page-section" id="services">
        <div class="container">
            <div class="text-center">
                <h2 class="section-heading text-uppercase ">
                    <i class='fa fa-list' style='font-size:48px;color:#a3d1d1'></i>
                    <font face="微軟正黑體" color="a3d1d1">展覽一覽表</font>
                </h2>
                <h3 class="section-subheading text-muted">
                    <font color="#ff9797">List of Exhibition</font>
                </h3>
                <h4 class="section-subheading text-muted">
                    <font color="#ff9797" face="微軟正黑體">您的主辦展覽</font>
                    <hr>
                    <table align=center cellpadding="50px" border="0">
                        <tr>
                            <th class="th-style1">展覽編號</th>
                            <th class="th-style2">展覽名稱</th>
                            <th class="th-style1">價格</th>
                        </tr>

                        <tr>
                            <td class="th-style1">123</td>
                            <td class="th-style2">國際電玩展</td>
                            <td class="th-style1">500 NT</td>
                            <td>
                                <a class="btn btn-primary2 btn-xl text-uppercase js-scroll-trigger"
                                    href="ex_modify.php">
                                    <font face="微軟正黑體" color="#fff">修改展覽</font>
                                </a>
                            </td>
                            <td>
                                <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger">
                                    <font face="微軟正黑體" color="white">刪除展覽</font>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="th-style1">123</td>
                            <td class="th-style2">國際電玩展</td>
                            <td class="th-style1">500 NT</td>
                            <td>
                                <a class="btn btn-primary2 btn-xl text-uppercase js-scroll-trigger"
                                    href="ex_modify.php">
                                    <font face="微軟正黑體" color="#fff">修改展覽</font>
                                </a>
                            </td>
                            <td>
                                <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger">
                                    <font face="微軟正黑體" color="white">刪除展覽</font>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="th-style1">123</td>
                            <td class="th-style2">國際電玩展</td>
                            <td class="th-style1">500 NT</td>
                            <td>
                                <a class="btn btn-primary2 btn-xl text-uppercase js-scroll-trigger"
                                    href="ex_modify.php">
                                    <font face="微軟正黑體" color="#fff">修改展覽</font>
                                </a>
                            </td>
                            <td>
                                <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger">
                                    <font face="微軟正黑體" color="white">刪除展覽</font>
                                </a>
                            </td>
                        </tr>
                    </table>
                    <hr>
                    <tr>
                </h4>
            </div>
        </div>
    </section>

    <!-- Footer-->
    <footer class="footer py-4">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-4 text-lg-left">Copyright © 覽得看_藝術展覽平台 2020</div>
                <div class="col-lg-4 my-3 my-lg-0">
                    <i class='fas fa-camera-retro' style="color: #ff9797;"></i>
                    <font face="微軟正黑體" color=#ff9797>_覽得看</font>
                </div>
            </div>
        </div>
    </footer>
    <!-- Bootstrap core JS-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
    <!-- Third party plugin JS-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
    <!-- Contact form JS-->
    <script src="assets/mail/jqBootstrapValidation.js"></script>
    <script src="assets/mail/contact_me.js"></script>
    <!-- Core theme JS-->
    <script src="js/scripts.js"></script>
</body>

</html>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>覽得看 - 美術類展覽</title>
    <link rel="icon" type="image/x-icon" href="assets/img/icon.jpg" />
    <!-- Font Awesome icons (free version)-->
    <script src="https://use.fontawesome.com/releases/v5.12.1/js/all.js" crossorigin="anonymous"></script>
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic" rel="stylesheet"
        type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css" />
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="css/styles.css" rel="stylesheet" />
    <link href="css/page.css" rel="stylesheet" />
</head>

<body id="page-top">
    <!-- Navigation-->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
        <div class="container">
            <a class="navbar-brand js-scroll-trigger" href="index.php"><i class='fas fa-camera-retro'></i>
                <font face="微軟正黑體">_覽得看</font>
            </a><button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">Menu<i class="fas fa-bars ml-1"></i></button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav text-uppercase ml-auto">
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#services">
                            <font face="微軟正黑體">登入</font>
                        </a></li>
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="category.php">
                            <font face="微軟正黑體">展覽類別</font>
                        </a></li>
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="">
                            <font face="微軟正黑體">願望清單</font>
                        </a></li>
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="">
                            <font face="微軟正黑體">購物車</font>
                        </a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- Masthead-->
    <header class="masthead2">
        <div class="container">
            <div class="masthead-heading text-uppercase">
                <font color="#00aaaa">&nbsp;&nbsp;&nbsp;Exhibitions</font>
                <font color=#ff9797>&nbsp;of Art</font>
            </div>
            <div class="masthead-heading text-uppercase">
                <font color="#ffffff" face="微軟正黑體">美術類</font>
            </div>
            <div class="wrap">
                <div class="search" style="margin:0px auto">
                    <input class="search-bar sign-up-form--select" type="text" name="search" id="search"
                        placeholder="輸入關鍵字" style="color:#00aaaa">
                    <button class="search-btn"><i class="fas fa-search" style="color:#ff9797"></i></button>
                </div>
            </div>
        </div>
    </header>
    <!-- Services-->
    <section class="page-section" id="services">
        <div class="container">
            <div class="text-center">
                <h2 class="section-heading text-uppercase ">
                    <i class='fas fa-palette' style='font-size:48px;color:#a3d1d1'></i>
                    <font face="微軟正黑體" color="a3d1d1">美術類</font>
                </h2>
                <h3 class="section-subheading text-muted">
                    <font color="#ff9797">Art</font>
                </h3>
                <h4 class="section-subheading text-muted">
                    <font color="#ff9797" face="微軟正黑體">_別具特色的畫作盡收眼底讓自己也成為一位小小美術家</font>
                </h4>
            </div>
        </div>
    </section>
    <!-- Portfolio Grid-->
    <section class="page-section bg-light" id="portfolio">
        <div class="container">
            <div class="text-center">
                <h2 class="section-heading text-uppercase">
                    <i class='fas fa-paint-brush' style='font-size:48px' color=#a3d1d1></i>
                    <font face="微軟正黑體" color="a3d1d1">美術類展覽</font>
                </h2>
                <h3 class="section-subheading text-muted">
                    <font color="#ff9797">Exhibitions of Art</font>
                </h3>
            </div>
            <div class="row">
                <?php foreach($event_art as $art) { ?>
                <div class="col-lg-4 col-sm-6 mb-4">
                    <div class="portfolio-item">
                        <a class="portfolio-link" data-toggle="modal" href="/artlist/<?=$art['id']?>">
                            <div class="portfolio-hover">
                                <div class="portfolio-hover-content"></div>
                            </div>
                            <img class="img-fluid" src="assets/img/展覽圖/<?=$art['img']?>.jpg" alt="" />
                        </a>
                        <div class="portfolio-caption">
                            <div class="portfolio-caption-heading">
                                <font face="微軟正黑體"><?=$art['name']?></font>
                            </div>
                            <div class="portfolio-caption-subheading text-muted">
                                <font color="#ff9797"><?=$art['eng_name']?></font>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>

            </div>
            <center>
                <a class="btn btn-primary2 btn-xl text-uppercase js-scroll-trigger" style="margin:30px"
                    href="ex_add.php">
                    <font face="微軟正黑體" color="#fff">新增展覽</font>
                </a>
            </center>
        </div>
    </section>
    <!-- Contact-->
    <section class="page-section" id="contact">
        <div class="container">
            <h2 class="section-heading text-uppercase" style="color: #00aaaa">
                <i class='fas fa-phone' style='font-size:48px'></i>
                聯絡我們</h2>
            <form id="contactForm" name="sentMessage" novalidate="novalidate">
                <div class="row align-items-stretch mb-5">
                    <div class="col-md-6">
                        <div class="form-group">
                            <input class="form-control" id="name" type="text" placeholder="Name *" required="required"
                                data-validation-required-message="Please enter your name." />
                            <p class="help-block text-danger"></p>
                        </div>
                        <div class="form-group">
                            <input class="form-control" id="email" type="email" placeholder="Email *"
                                required="required"
                                data-validation-required-message="Please enter your email address." />
                            <p class="help-block text-danger"></p>
                        </div>
                        <div class="form-group mb-md-0">
                            <input class="form-control" id="phone" type="tel" placeholder="Phone Number *"
                                required="required"
                                data-validation-required-message="Please enter your phone number." />
                            <p class="help-block text-danger"></p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group form-group-textarea mb-md-0">
                            <textarea class="form-control" id="message" placeholder="Message *" required="required"
                                data-validation-required-message="Please enter a message."></textarea>
                            <p class="help-block text-danger"></p>
                        </div>
                    </div>
                </div>
                <div class="text-center">
                    <div id="success"></div>
                    <button class="btn btn-primary btn-xl text-uppercase" id="sendMessageButton" type="submit">Send
                        Message</button>
                </div>
            </form>
        </div>
    </section>
    <!-- Footer-->
    <footer class="footer py-4">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-4 text-lg-left">Copyright © 覽得看_藝術展覽平台 2020</div>
                <div class="col-lg-4 my-3 my-lg-0">
                    <i class='fas fa-camera-retro' style="color: #ff9797;"></i>
                    <font face="微軟正黑體" color=#ff9797>_覽得看</font>
                </div>
            </div>
        </div>
    </footer>
    <!-- Bootstrap core JS-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
    <!-- Third party plugin JS-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
    <!-- Contact form JS-->
    <script src="assets/mail/jqBootstrapValidation.js"></script>
    <script src="assets/mail/contact_me.js"></script>
    <!-- Core theme JS-->
    <script src="js/scripts.js"></script>
</body>

</html>
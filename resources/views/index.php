<?php

session_start();
$_SESSION["venId"] = $ven_id;
if ($cus_id) {
    $_SESSION["cusId"] = $cus_id;
    $Id = $_SESSION["cusId"];
} else {
    $_SESSION["venId"] = $ven_id;
    $Id = $_SESSION["venId"];
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>覽得看 - 藝術展覽平台</title>
    <link rel="icon" type="image/x-icon" href="assets/img/icon.jpg" />
    <!-- Font Awesome icons (free version)-->
    <script src="https://use.fontawesome.com/releases/v5.12.1/js/all.js" crossorigin="anonymous"></script>
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic" rel="stylesheet"
        type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css" />
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="css/styles.css" rel="stylesheet" />
    <link href="css/page.css" rel="stylesheet" />
</head>

<body id="page-top">
    <!-- Navigation-->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
        <div class="container">
            <a class="navbar-brand js-scroll-trigger" href="#page-top"><i class='fas fa-camera-retro'></i>
                <font face="微軟正黑體">_覽得看</font>
            </a><button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">Menu<i class="fas fa-bars ml-1"></i></button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav text-uppercase ml-auto">
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="logout_process.php">
                            <font face="微軟正黑體">Hello,<?=$name?></font>
                        </a></li>
                    <?php if ($Id) { ?>
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="logout_process.php">
                            <font face="微軟正黑體">登出</font>
                        </a></li>
                    <?php } else { ?>
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#services">
                            <font face="微軟正黑體">登入</font>
                        </a></li>
                    <?php } ?>
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#portfolio">
                            <font face="微軟正黑體">展覽類別</font>
                        </a></li>
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="bucketlist.php">
                            <font face="微軟正黑體">願望清單</font>
                        </a></li>
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="cart.php">
                            <font face="微軟正黑體">購物車</font>
                        </a></li>
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="own_ex.php">
                            <font face="微軟正黑體">主辦展覽</font>
                        </a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- Masthead-->
    <header class="masthead">
        <div class="container">
            <div class="masthead-subheading">Become a Life Artist !</div>
            <div class="masthead-heading text-uppercase">ENRICHING YOUR LIFE</div>
        </div>
    </header>
    <!-- Services-->
    <?php if (!$Id) {?>
    <section class="page-section" id="services">
        <div class="container">
            <div class="text-center">
                <h2 class="section-heading text-uppercase ">
                    <i class='far fa-address-card' style='font-size:48px' color=#a3d1d1></i>
                    <span class="section-heading-text">登入</span>
                </h2>
                <h3 class="section-subheading text-muted">
                    <font color="#ff9797">Login</font>
                </h3>
            </div>
            <div class="row text-center">
                <div class="col-md-4">
                    <i class='fas fa-walking' style='font-size:48px'></i>
                    <h4 class="my-3">
                        <font face="微軟正黑體">參展者</font>
                    </h4>
                    <p class="text-muted">
                        <font face="微軟正黑體">利用參展者身分登入即可使用購<br>物車、購票功能與願望清單</font>
                    </p>
                    <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" href="cus_login.php">
                        <font color="#fff">Login</font>
                    </a>
                </div>
                <div class="col-md-4">
                    <i class='far fa-calendar-alt' style='font-size:48px'></i>
                    <h4 class="my-3">
                        <font face="微軟正黑體">主辦方</font>
                    </h4>
                    <p class="text-muted">
                        <font face="微軟正黑體">利用主辦方身分登入即可使用<br>展覽新增、修改與刪除功能</font>
                    </p>
                    <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" href="ven_login.php">
                        <font color="#fff">Login</font>
                    </a>
                </div>
                <div class="col-md-4">
                    <i class='fas fa-edit' style='font-size:48px'></i>
                    <h4 class="my-3">
                        <font face="微軟正黑體">管理者</font>
                    </h4>
                    <p class="text-muted">
                        <font face="微軟正黑體">利用管理者身分登入即可取<br>得展覽門票相關之訂單資訊</font>
                    </p>
                    <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" href="mag_login.php">
                        <font color="#fff">Login</font>
                    </a>
                </div>
            </div>
            <div class="sign-up--font" style="text-align: right; margin: 50px; color: #a3d1d1;">
                尚未擁有帳號嗎?前往註冊
                <i class='fas fa-angle-double-right' style='font-size:36px'></i>
                <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" href="signup_index.php">
                    <font color="#fff">註冊</font>
                </a>
            </div>
        </div>
    </section>
    <?php } ?>
    <!-- Portfolio Grid-->
    <section class="page-section bg-light" id="portfolio">
        <div class="container">
            <div class="text-center">
                <h2 class="section-heading text-uppercase">
                    <i class='fas fa-folder-open' style='font-size:48px' color=#a3d1d1></i>
                    展覽類別
                </h2>
                <h3 class="section-subheading text-muted">
                    <font color="#ff9797">Category</font>
                </h3>
            </div>
            <div class="row">
                <div class="col-lg-4 col-sm-6 mb-4">
                    <div class="portfolio-item">
                        <a href="culturelist.php">
                            <div class="portfolio-hover">
                                <div class="portfolio-hover-content"></div>
                            </div>
                            <img class="img-fluid" src="assets/img/category/culture.jpg" alt="" />
                        </a>
                        <div class="portfolio-caption">
                            <div class="portfolio-caption-heading">
                                <font face="微軟正黑體">文化類</font>
                            </div>
                            <div class="portfolio-caption-subheading text-muted">
                                <font color="#ff9797">Culture</font>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 mb-4">
                    <div class="portfolio-item">
                        <a href="foodlist.php">
                            <div class="portfolio-hover">
                                <div class="portfolio-hover-content"></div>
                            </div>
                            <img class="img-fluid" src="assets/img/category/food.jpg" alt="" />
                        </a>
                        <div class="portfolio-caption">
                            <div class="portfolio-caption-heading">
                                <font face="微軟正黑體">食品類</font>
                            </div>
                            <div class="portfolio-caption-subheading text-muted">
                                <font color="#ff9797">Foodstuff</font>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 mb-4">
                    <div class="portfolio-item">
                        <a href="artlist.php">
                            <div class="portfolio-hover">
                                <div class="portfolio-hover-content"></div>
                            </div>
                            <img class="img-fluid" src="assets/img/category/art.jpg" alt="" />
                        </a>
                        <div class="portfolio-caption">
                            <div class="portfolio-caption-heading">
                                <font face="微軟正黑體">美術類</font>
                            </div>
                            <div class="portfolio-caption-subheading text-muted">
                                <font color="#ff9797">Art</font>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 mb-4 mb-lg-0">
                    <div class="portfolio-item">
                        <a href="sciencelist.php">
                            <div class="portfolio-hover">
                                <div class="portfolio-hover-content"></div>
                            </div>
                            <img class="img-fluid" src="assets/img/category/technology.jpg" alt="" />
                        </a>
                        <div class="portfolio-caption">
                            <div class="portfolio-caption-heading">
                                <font face="微軟正黑體">科技類</font>
                            </div>
                            <div class="portfolio-caption-subheading text-muted">
                                <font color="#ff9797">Science and Technology</font>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 mb-4 mb-sm-0">
                    <div class="portfolio-item">
                        <a href="designlist.php">
                            <div class="portfolio-hover">
                                <div class="portfolio-hover-content"></div>
                            </div>
                            <img class="img-fluid" src="assets/img/category/design.jpg" alt="" />
                        </a>
                        <div class="portfolio-caption">
                            <div class="portfolio-caption-heading">
                                <font face="微軟正黑體">設計類</font>
                            </div>
                            <div class="portfolio-caption-subheading text-muted">
                                <font color="#ff9797">Design</font>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <div class="portfolio-item">
                        <a href="ecolist.php">
                            <div class="portfolio-hover">
                                <div class="portfolio-hover-content"></div>
                            </div>
                            <img class="img-fluid" src="assets/img/category/ecology.jpg" alt="" />
                        </a>
                        <div class="portfolio-caption">
                            <div class="portfolio-caption-heading">
                                <font face="微軟正黑體">生態類</font>
                            </div>
                            <div class="portfolio-caption-subheading text-muted">
                                <font color="#ff9797">Ecology</font>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- About-->
    <section class="page-section" id="about">
        <div class="container">
            <div class="text-center">
                <h2 class="section-heading text-uppercase">
                    <i class='fas fa-bullhorn' style='font-size:48px; color: #a3d1d1'></i>
                    推薦展覽
                </h2>
                <h3 class="section-subheading text-muted">
                    <font color="#ff9797">Recommended Exhibition</font>
                </h3>
            </div>
            <ul class="timeline">
                <li>
                    <div class="timeline-image"><img class="rounded-circle img-fluid" src="assets/img/展覽圖/設計類/建築設計展.jpg"
                            alt="" /></div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4>
                                <i class='fas fa-pencil-ruler' style='font-size:24px;color:#ff9797'></i>
                                <font face="微軟正黑體" color=#ff9797>設計類</font>
                            </h4>
                            <h4 class="subheading">
                                <font face="微軟正黑體" color=#00aaaa>建築設計展</font>
                            </h4>
                        </div>
                        <div class="timeline-body">
                            <p class="text-muted1">各國特殊造景建築盡收眼底，展示了各國名建築師所設計之藍圖與建築模型，
                                讓設計人大開眼界，快來一探建築設計的世界吧 !
                            </p>
                        </div>
                    </div>
                </li>
                <li class="timeline-inverted">
                    <div class="timeline-image"><img class="rounded-circle img-fluid" src="assets/img/展覽圖/文化類/歐洲展.jpg"
                            alt="" /></div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4>
                                <i class='fas fa-globe-americas' style='font-size:24px;color:#00aaaa'></i>
                                <font face="微軟正黑體" color=#00aaaa>文化類</font>
                            </h4>
                            <h4 class="subheading">
                                <font face="微軟正黑體" color=#ff9797>歐洲文化展</font>
                            </h4>
                        </div>
                        <div class="timeline-body">
                            <p class="text-muted2">遙遠的歐洲國度，彷彿隔著一層充滿文藝的神秘面紗，歐式美食、古希臘式
                                建築與深邃的高加索臉孔，對我們而言都是如此陌生與好奇的存在，來一場歐式風情的浪漫派對吧 !
                            </p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="timeline-image"><img class="rounded-circle img-fluid" src="assets/img/展覽圖/美術類/皇民化畫展.jpg"
                            alt="" /></div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4>
                                <i class='fas fa-palette' style='font-size:24px;color:#ff9797'></i>
                                <font face="微軟正黑體" color=#ff9797>美術類</font>
                            </h4>
                            <h4 class="subheading">
                                <font face="微軟正黑體" color=#00aaaa>皇民化畫展</font>
                            </h4>
                        </div>
                        <div class="timeline-body">
                            <p class="text-muted1">日治50年，日本文化對1900年代初的台灣影響無遠弗屆，
                                包含了戶政系統、時間觀念、法律紀律甚至是美術文化與風格，皆能顯現出當時大和文化的蹤跡，
                                融入皇民化畫展，共同瞭解皇民化時期畫家的心境與不能說出口的心聲 !
                            </p>
                        </div>
                    </div>
                </li>
                <li class="timeline-inverted">
                    <div class="timeline-image"><img class="rounded-circle img-fluid" src="assets/img/展覽圖/科技展/醫療科技展.jpg"
                            alt="" /></div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4>
                                <i class='fas fa-satellite-dish' style='font-size:24px;color:#00aaaa'></i>
                                <font face="微軟正黑體" color=#00aaaa>科技類</font>
                            </h4>
                            <h4 class="subheading">
                                <font face="微軟正黑體" color=#ff9797>醫療科技展</font>
                            </h4>
                        </div>
                        <div class="timeline-body">
                            <p class="text-muted2">科技是現代社會進步的佐證，醫療是促進現代社會進步的能源，兩者皆是
                                人類文化前進的基礎，醫療科技一詞或許甚是陌生，但生老病死與便利科技卻是人們天天皆在接觸的
                                ，讓醫療科技展帶領你探索你所不知的科技世界 !
                            </p>
                        </div>
                    </div>
                </li>
                <li class="timeline-inverted">
                    <div class="timeline-image">
                        <h4>Start<br />Your Life<br />Story!</h4>
                    </div>
                </li>
            </ul>
        </div>
    </section>

    <!-- Contact-->
    <section class="page-section" id="contact">
        <div class="container">
            <h2 class="section-heading text-uppercase" style="color: #00aaaa">
                <i class='fas fa-phone' style='font-size:48px'></i>
                聯絡我們</h2>
            <form id="contactForm" name="sentMessage" novalidate="novalidate">
                <div class="row align-items-stretch mb-5">
                    <div class="col-md-6">
                        <div class="form-group">
                            <input class="form-control" id="name" type="text" placeholder="Name *" required="required"
                                data-validation-required-message="Please enter your name." />
                            <p class="help-block text-danger"></p>
                        </div>
                        <div class="form-group">
                            <input class="form-control" id="email" type="email" placeholder="Email *"
                                required="required"
                                data-validation-required-message="Please enter your email address." />
                            <p class="help-block text-danger"></p>
                        </div>
                        <div class="form-group mb-md-0">
                            <input class="form-control" id="phone" type="tel" placeholder="Phone Number *"
                                required="required"
                                data-validation-required-message="Please enter your phone number." />
                            <p class="help-block text-danger"></p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group form-group-textarea mb-md-0">
                            <textarea class="form-control" id="message" placeholder="Message *" required="required"
                                data-validation-required-message="Please enter a message."></textarea>
                            <p class="help-block text-danger"></p>
                        </div>
                    </div>
                </div>
                <div class="text-center">
                    <div id="success"></div>
                    <button class="btn btn-primary btn-xl text-uppercase" id="sendMessageButton" type="submit">Send
                        Message</button>
                </div>
            </form>
        </div>
    </section>
    <!-- Footer-->
    <footer class="footer py-4">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-4 text-lg-left">Copyright © 覽得看_藝術展覽平台 2020</div>
                <div class="col-lg-4 my-3 my-lg-0">
                    <i class='fas fa-camera-retro' style="color: #ff9797;"></i>
                    <font face="微軟正黑體" color=#ff9797>_覽得看</font>
                </div>
            </div>
        </div>
    </footer>
    <!-- Portfolio Modals-->
    <!-- Modal 1-->
    <div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal"><img src="/assets/img/close-icon.svg" /></div>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8">
                            <div class="modal-body">
                                <!-- Project Details Go Here-->
                                <h2 class="text-uppercase">
                                    <font face="微軟正黑體" color="a3d1d1">文化類</font>
                                </h2>
                                <font color="#ff9797">Culture</font>
                                <li>體驗各國特有風俗民情與少數民族特色歷史文化</li>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-lg-4 col-sm-6 mb-4">
                                            <div class="portfolio-item">
                                                <a class="portfolio-link" data-toggle="modal" href="#portfolioModal1">
                                                    <img class="img-fluid" src="assets/img/category/culture.jpg"
                                                        alt="" />
                                                </a>
                                                <div class="portfolio-caption">
                                                    <div class="portfolio-caption-heading">
                                                        <font face="微軟正黑體">文化類</font>
                                                    </div>
                                                    <div class="portfolio-caption-subheading text-muted">
                                                        <font color="#ff9797">Culture</font>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-sm-6 mb-4">
                                            <div class="portfolio-item">
                                                <a class="portfolio-link" data-toggle="modal" href="#portfolioModal2">
                                                    <img class="img-fluid" src="assets/img/category/food.jpg"
                                                        alt="" /></a>
                                                <div class="portfolio-caption">
                                                    <div class="portfolio-caption-heading">
                                                        <font face="微軟正黑體">食品類</font>
                                                    </div>
                                                    <div class="portfolio-caption-subheading text-muted">
                                                        <font color="#ff9797">Foodstuff</font>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-sm-6 mb-4">
                                            <div class="portfolio-item">
                                                <a class="portfolio-link" data-toggle="modal" href="#portfolioModal3">
                                                    <img class="img-fluid" src="assets/img/category/art.jpg"
                                                        alt="" /></a>
                                                <div class="portfolio-caption">
                                                    <div class="portfolio-caption-heading">
                                                        <font face="微軟正黑體">美術類</font>
                                                    </div>
                                                    <div class="portfolio-caption-subheading text-muted">
                                                        <font color="#ff9797">Art</font>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-sm-6 mb-4 mb-lg-0">
                                            <div class="portfolio-item">
                                                <a class="portfolio-link" data-toggle="modal" href="#portfolioModal4">
                                                    <img class="img-fluid" src="assets/img/category/technology.jpg"
                                                        alt="" /></a>
                                                <div class="portfolio-caption">
                                                    <div class="portfolio-caption-heading">
                                                        <font face="微軟正黑體">科技類</font>
                                                    </div>
                                                    <div class="portfolio-caption-subheading text-muted">
                                                        <font color="#ff9797">Science and Technology</font>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-sm-6 mb-4 mb-sm-0">
                                            <div class="portfolio-item">
                                                <a class="portfolio-link" data-toggle="modal" href="#portfolioModal5">
                                                    <img class="img-fluid" src="assets/img/category/design.jpg"
                                                        alt="" /></a>
                                                <div class="portfolio-caption">
                                                    <div class="portfolio-caption-heading">
                                                        <font face="微軟正黑體">設計類</font>
                                                    </div>
                                                    <div class="portfolio-caption-subheading text-muted">
                                                        <font color="#ff9797">Design</font>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-sm-6">
                                            <div class="portfolio-item">
                                                <a class="portfolio-link" data-toggle="modal" href="#portfolioModal6">
                                                    <img class="img-fluid" src="assets/img/category/ecology.jpg"
                                                        alt="" /></a>
                                                <div class="portfolio-caption">
                                                    <div class="portfolio-caption-heading">
                                                        <font face="微軟正黑體">生態類</font>
                                                    </div>
                                                    <div class="portfolio-caption-subheading text-muted">
                                                        <font color="#ff9797">Ecology</font>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </section>
                                </ul>
                                <br>
                                <button class="btn btn-primary" data-dismiss="modal" type="button"><i
                                        class="fas fa-times mr-1"></i>Close Project</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal 2-->
    <div class="portfolio-modal modal fade" id="portfolioModal2" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal"><img src="assets/img/close-icon.svg" /></div>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8">
                            <div class="modal-body">
                                <!-- Project Details Go Here-->
                                <h2 class="text-uppercase">
                                    <font face="微軟正黑體" color="a3d1d1">食品類</font>
                                </h2>
                                <font color="#ff9797">Foodstuff</font>
                                <li>體驗各國特有風俗民情與少數民族特色歷史文化</li>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-lg-4 col-sm-6 mb-4">
                                            <div class="portfolio-item">
                                                <a class="portfolio-link" data-toggle="modal" href="#portfolioModal1">
                                                    <img class="img-fluid" src="assets/img/category/culture.jpg"
                                                        alt="" />
                                                </a>
                                                <div class="portfolio-caption">
                                                    <div class="portfolio-caption-heading">
                                                        <font face="微軟正黑體">文化類</font>
                                                    </div>
                                                    <div class="portfolio-caption-subheading text-muted">
                                                        <font color="#ff9797">Culture</font>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-sm-6 mb-4">
                                            <div class="portfolio-item">
                                                <a class="portfolio-link" data-toggle="modal" href="#portfolioModal2">
                                                    <img class="img-fluid" src="assets/img/category/food.jpg"
                                                        alt="" /></a>
                                                <div class="portfolio-caption">
                                                    <div class="portfolio-caption-heading">
                                                        <font face="微軟正黑體">食品類</font>
                                                    </div>
                                                    <div class="portfolio-caption-subheading text-muted">
                                                        <font color="#ff9797">Foodstuff</font>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-sm-6 mb-4">
                                            <div class="portfolio-item">
                                                <a class="portfolio-link" data-toggle="modal" href="#portfolioModal3">
                                                    <img class="img-fluid" src="assets/img/category/art.jpg"
                                                        alt="" /></a>
                                                <div class="portfolio-caption">
                                                    <div class="portfolio-caption-heading">
                                                        <font face="微軟正黑體">美術類</font>
                                                    </div>
                                                    <div class="portfolio-caption-subheading text-muted">
                                                        <font color="#ff9797">Art</font>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-sm-6 mb-4 mb-lg-0">
                                            <div class="portfolio-item">
                                                <a class="portfolio-link" data-toggle="modal" href="#portfolioModal4">
                                                    <img class="img-fluid" src="assets/img/category/technology.jpg"
                                                        alt="" /></a>
                                                <div class="portfolio-caption">
                                                    <div class="portfolio-caption-heading">
                                                        <font face="微軟正黑體">科技類</font>
                                                    </div>
                                                    <div class="portfolio-caption-subheading text-muted">
                                                        <font color="#ff9797">Science and Technology</font>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-sm-6 mb-4 mb-sm-0">
                                            <div class="portfolio-item">
                                                <a class="portfolio-link" data-toggle="modal" href="#portfolioModal5">
                                                    <img class="img-fluid" src="assets/img/category/design.jpg"
                                                        alt="" /></a>
                                                <div class="portfolio-caption">
                                                    <div class="portfolio-caption-heading">
                                                        <font face="微軟正黑體">設計類</font>
                                                    </div>
                                                    <div class="portfolio-caption-subheading text-muted">
                                                        <font color="#ff9797">Design</font>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-sm-6">
                                            <div class="portfolio-item">
                                                <a class="portfolio-link" data-toggle="modal" href="#portfolioModal6">
                                                    <img class="img-fluid" src="assets/img/category/ecology.jpg"
                                                        alt="" /></a>
                                                <div class="portfolio-caption">
                                                    <div class="portfolio-caption-heading">
                                                        <font face="微軟正黑體">生態類</font>
                                                    </div>
                                                    <div class="portfolio-caption-subheading text-muted">
                                                        <font color="#ff9797">Ecology</font>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <button class="btn btn-primary" data-dismiss="modal" type="button"><i
                                        class="fas fa-times mr-1"></i>Close Project</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal 3-->
    <div class="portfolio-modal modal fade" id="portfolioModal3" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal"><img src="assets/img/close-icon.svg" /></div>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8">
                            <div class="modal-body">
                                <!-- Project Details Go Here-->
                                <h2 class="text-uppercase">
                                    <font face="微軟正黑體" color="a3d1d1">美術類</font>
                                </h2>
                                <p class="item-intro text-muted">
                                    <font color="#ff9797">Art</font>
                                </p>
                                <img class="img-fluid d-block mx-auto" src="assets/img/portfolio/03-full.jpg" alt="" />
                                <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur
                                    adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt
                                    repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae,
                                    nostrum, reiciendis facere nemo!</p>
                                <ul class="list-inline">
                                    <li>Date: January 2020</li>
                                    <li>Client: Finish</li>
                                    <li>Category: Identity</li>
                                </ul>
                                <button class="btn btn-primary" data-dismiss="modal" type="button"><i
                                        class="fas fa-times mr-1"></i>Close Project</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal 4-->
    <div class="portfolio-modal modal fade" id="portfolioModal4" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal"><img src="assets/img/close-icon.svg" /></div>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8">
                            <div class="modal-body">
                                <!-- Project Details Go Here-->
                                <h2 class="text-uppercase">
                                    <font face="微軟正黑體" color="a3d1d1">科技類</font>
                                </h2>
                                <p class="item-intro text-muted">
                                    <font color="#ff9797">Science and Technology</font>
                                </p>
                                <img class="img-fluid d-block mx-auto" src="assets/img/portfolio/04-full.jpg" alt="" />
                                <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur
                                    adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt
                                    repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae,
                                    nostrum, reiciendis facere nemo!</p>
                                <ul class="list-inline">
                                    <li>Date: January 2020</li>
                                    <li>Client: Lines</li>
                                    <li>Category: Branding</li>
                                </ul>
                                <button class="btn btn-primary" data-dismiss="modal" type="button"><i
                                        class="fas fa-times mr-1"></i>Close Project</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal 5-->
    <div class="portfolio-modal modal fade" id="portfolioModal5" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal"><img src="assets/img/close-icon.svg" /></div>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8">
                            <div class="modal-body">
                                <!-- Project Details Go Here-->
                                <h2 class="text-uppercase">
                                    <font face="微軟正黑體" color="a3d1d1">設計類</font>
                                </h2>
                                <p class="item-intro text-muted">
                                    <font color="#ff9797">Design</font>
                                </p>
                                <img class="img-fluid d-block mx-auto" src="assets/img/portfolio/05-full.jpg" alt="" />
                                <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur
                                    adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt
                                    repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae,
                                    nostrum, reiciendis facere nemo!</p>
                                <ul class="list-inline">
                                    <li>Date: January 2020</li>
                                    <li>Client: Southwest</li>
                                    <li>Category: Website Design</li>
                                </ul>
                                <button class="btn btn-primary" data-dismiss="modal" type="button"><i
                                        class="fas fa-times mr-1"></i>Close Project</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal 6-->
    <div class="portfolio-modal modal fade" id="portfolioModal6" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal"><img src="assets/img/close-icon.svg" /></div>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8">
                            <div class="modal-body">
                                <!-- Project Details Go Here-->
                                <h2 class="text-uppercase">
                                    <font face="微軟正黑體" color="a3d1d1">生態類</font>
                                </h2>
                                <p class="item-intro text-muted">
                                    <font color="#ff9797">Ecology</font>
                                </p>
                                <img class="img-fluid d-block mx-auto" src="assets/img/portfolio/06-full.jpg" alt="" />
                                <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur
                                    adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt
                                    repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae,
                                    nostrum, reiciendis facere nemo!</p>
                                <ul class="list-inline">
                                    <li>Date: January 2020</li>
                                    <li>Client: Window</li>
                                    <li>Category: Photography</li>
                                </ul>
                                <button class="btn btn-primary" data-dismiss="modal" type="button"><i
                                        class="fas fa-times mr-1"></i>Close Project</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Bootstrap core JS-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
    <!-- Third party plugin JS-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
    <!-- Contact form JS-->
    <script src="assets/mail/jqBootstrapValidation.js"></script>
    <script src="assets/mail/contact_me.js"></script>
    <!-- Core theme JS-->
    <script src="js/scripts.js"></script>
</body>

</html>
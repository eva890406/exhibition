<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>覽得看 - 新增展覽</title>
    <link rel="icon" type="image/x-icon" href="assets/img/icon.jpg" />
    <!-- Font Awesome icons (free version)-->
    <script src="https://use.fontawesome.com/releases/v5.12.1/js/all.js" crossorigin="anonymous"></script>
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic" rel="stylesheet"
        type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css" />
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="css/styles.css" rel="stylesheet" />
    <link href="css/page.css" rel="stylesheet" />
</head>

<body id="page-top">
    <!-- Navigation-->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
        <div class="container">
            <a class="navbar-brand js-scroll-trigger" href="index.php"><i class='fas fa-camera-retro'></i>
                <font face="微軟正黑體">_覽得看</font>
            </a><button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">Menu<i class="fas fa-bars ml-1"></i></button>


            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav text-uppercase ml-auto">
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="login_index.php">
                            <font face="微軟正黑體">登入</font>
                        </a></li>
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="category.php">
                            <font face="微軟正黑體">展覽類別</font>
                        </a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- Masthead-->
    <header class="masthead2">
        <div class="masthead-heading text-uppercase">
            <font color="#00aaaa">&emsp;Add New</font>
            <font color=#ff9797>&nbsp;Exhibition</font>
        </div>
        <div class="masthead-heading text-uppercase">
            <font color="#ffffff" face="微軟正黑體">新增展覽</font>
        </div>
        <div class="row text-center">
        <?= $msg ?>
        <form action="/events/add" method="post">
        <div class="row text-center">
            <div class="col-md-4-2">
                <font color="#00aaaa">
                    主辦編號:&nbsp;<input type="text" class="sign-up-form--select2" name="vendors_member_id"
                        style="margin:10px; width:100px">
                    <br>
                    地區編號:&nbsp;<input type="text" class="sign-up-form--select2" name="area_id"
                        style="margin:10px; width:100px">
                    <br>
                    分類編號:&nbsp;<input type="text" class="sign-up-form--select2" name="events_category_id"
                        style="margin:10px; width:100px">
                    <br>
                </font>
            </div>
            <div class="col-md-4-2">
                <font color="#ff9797">
                    展覽名稱:&nbsp;<input type="text" class="sign-up-form--select2" name="name" style="margin:10px">
                    <br>
                    展覽時間:&nbsp;<input type="text" class="sign-up-form--select2" name="time" style="margin:10px">
                    <br>
                    門票價格:&nbsp;<input type="text" class="sign-up-form--select2" name="price" style="margin:10px">
                    <br>
                    展覽地點:&nbsp;<input type="text" class="sign-up-form--select2" name="location" style="margin:10px">
                    <br>
                    展覽介紹:&nbsp;
                    <br>&emsp;&emsp;&emsp;&emsp;
                    <textarea name="describtion" style="font-family:微軟正黑體; color:#ff9797">*請輸入展覽簡介</textarea>
                </font>
            </div>
        </div>
        <center>
            <input class="sign-up-form--submit-button" type="submit" value="確認新增">
        </center>
        </form>
    </header>

    <!-- Footer-->
    <footer class="footer py-4">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-4 text-lg-left">Copyright © 覽得看_藝術展覽平台 2020</div>
                <div class="col-lg-4 my-3 my-lg-0">
                    <i class='fas fa-camera-retro' style="color: #ff9797;"></i>
                    <font face="微軟正黑體" color=#ff9797>_覽得看</font>
                </div>
            </div>
        </div>
    </footer>
    <!-- Bootstrap core JS-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
    <!-- Third party plugin JS-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
    <!-- Contact form JS-->
    <script src="assets/mail/jqBootstrapValidation.js"></script>
    <script src="assets/mail/contact_me.js"></script>
    <!-- Core theme JS-->
    <script src="js/scripts.js"></script>
</body>

</html>
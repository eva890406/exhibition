<?php

/**
* =============================================================================
* = 驗證乘客登入
* =============================================================================
*
* @param String $account  
* @param String $password  
* @return Boolean
*
**/
function verifyConsumerLogin($account, $password) {
    $user = DB::find('consumers_member', $account, 'account');

    if($user && $user['password'] == $password) {
        return true;
    }
    return false;
}

function verifyVendorLogin($account, $password) {
    $user = DB::find('vendors_member', $account, 'account');

    if($user && $user['password'] == $password){ return true;}
    return false;
}

function verifyManagerLogin($account, $password) {
    $user = DB::find('system_manager', $account, 'account');

    if($user && $user['password'] == $password) return true;
    return false;
}

/*
$d_account = $_SESSION['d_account'] ?? '';
$departtime = findDriverByBUS($conn, $d_account);
$time=updatedepart($conn, $d_account, $data=[]);
$arrive=date("Y-m-d", strtotime($time."+10 minute"));
*/
/**
* =============================================================================
* = bucketlist 
* =============================================================================
*
* @param DateTime $departTime
* @return DateTime
*
**/
function bucketlist_Checkoutdelete($favoriteItem) {
    $times=0;
    $data=[];
    foreach($favoriteItem as $item){
        $event = DB::find('events', $item['events_id'], 'id');
        //var_dump($eventId);
        $data[$times] = $event;
        $times+=1;
    }
    //var_dump($eventId);die;
    return $data;
}

function singlebucket($item){
    $times=0;
    $data=[];
    foreach($item as $i){
        $data[$times] = $i;
        $times+=1; 
    }

    return $data;
}
function cart_checkout($favoriteItem) {
    $times=0;
    $data=[];
    foreach($favoriteItem as $item){
        $event = DB::find('events', $item['events_id'], 'id');
        //var_dump($eventId);
        $times+=1;
        $data[$times] = $event;
    }
    //var_dump($eventId);die;
    return $data;
}
/*
function BucketToCart($event,$cusId){

    $data = ['events_id' => $event['id'],
    'consumers_member_id' => $cusId,
    'quntity' => $cusId,
    'price' => $cusId,
        'create_at' => date("Y-m-d H:i:s"),]
}*/
/**
* =============================================================================
* = 預估抵達下站時間
* =============================================================================
*
* @param DateTime $departTime
* @return DateTime
*
**/
function countTimeToArriveNextStop($departTime) {

    return date("Y-m-d H:i:s", strtotime($departTime."+10 minute"));
}
<?php
//web期末
//require __DIR__ . '/../models/consumers_member.php';
//require __DIR__ . '/../models/events_category.php';
//require __DIR__ . '/../models/vendors_member.php';

// =============================================================================
// = Users
// =============================================================================
// 
/**
 * 獲取所有欄位名稱
 *
 * @param  PDO $conn     PDO實體
 * @param  array $data   要新增的使用者資料
 * @return boolean       執行結果
 */
$queryAllExhibtion  = "SELECT * FROM exhibtion";
function fetchClass($conn, $sql)
{
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}
/**測試完成
 * 取得所有使用者
 *
 * @param  PDO $conn    PDO實體
 * @return object
 */

//取得所有comsumers
function fetchAllComsumers($conn)
{
    $stmt = $conn->prepare('SELECT * FROM consumers_menber');
    $stmt->execute();
    
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}


// * 依照給予的帳號，取得comsumers done

function findComsumerByAccount($conn, $account)
{
    
    $stmt = $conn->prepare('SELECT * FROM `consumers_member` where csm_ac = :account');
    $stmt->bindParam(':account', $account);
    $stmt->execute();

    return $stmt->fetch(PDO::FETCH_ASSOC);
}

//register
function createConsumer($conn, $data = [])
{
    $sql = "INSERT INTO consumers_member(csm_name, csm_ac, csm_pw, csm_dob, csm_gender, csm_age) values (:csm_name, :csm_ac, :csm_pw, :csm_dob, :csm_gender, :csm_age)";
    $stmt = $conn->prepare($sql);

    $addConsumerData = [
        'csm_name'       => $data['csm_name'],
        'csm_ac'    => $data['csm_ac'],
        'csm_pw'   => $data['csm_pw'],
        'csm_dob'       => $data['csm_dob'],
        'csm_gender' => $data['csm_gender'],
        'csm_age' => $data['csm_age'] ,
    ];
    
    return $stmt->execute($addConsumerData);
}

//
function findConsumerMaxId($conn)
{
    $stmt = $conn->prepare('SELECT Max(csm_id) FROM `consumers_member`');
    $stmt->execute();
    
    return $stmt->fetch(PDO::FETCH_ASSOC);
}

////////////////////////////////////////
function fetchAllConsumers_member($conn)
{
    $stmt = $conn->prepare('SELECT * FROM `consumers_member`');
    $stmt->execute();

    return $stmt->fetchAll(PDO::FETCH_CLASS, 'CsmUsers');
}
function fetchAllVendor_member($conn)
{
    $stmt = $conn->prepare('SELECT * FROM `events_category `');
    $stmt->execute();

    return $stmt->fetchAll(PDO::FETCH_CLASS, 'CtgUsers');
}

//取的系統管理員的帳號
function findManagerByAccount($conn, $account)
{
     
    $stmt = $conn->prepare('SELECT * FROM `system_manager` where sm_ac = :account');
    $stmt->bindParam(':account', $account);
    $stmt->execute();

    return $stmt->fetch(PDO::FETCH_ASSOC);
}

//取得all vendormembers
function fetchAllvendors_member($conn)
{
    $stmt = $conn->prepare('SELECT * FROM `vendors_member`');
    $stmt->execute();

    return $stmt->fetchAll(PDO::FETCH_CLASS, 'VdmUsers');
}

//取的vendor members 的帳號
function findV_membersByAccount($conn, $account)
{
    
    $stmt = $conn->prepare('SELECT * FROM `vendors_member` where vd_ac = :account');
    $stmt->bindParam(':account', $account);
    $stmt->execute();

    return $stmt->fetch(PDO::FETCH_ASSOC);
}
/**
 * 更新使用者願望清單資料
 *
 * @param  PDO $conn     PDO實體
 * @param  string $id    要更新清單的使用者編號
 * @param  array $data   更新後的願望清單資料
 * @return boolean       執行結果
 */
function findWishList($conn, $fitem_code, $evt_code)
{
    $stmt = $conn->prepare(
        "SELECT * FROM `favorite_item` WHERE `fitem_code`= $fitem_code AND `evt_code`= $evt_code"
    );

    $stmt->execute();

    return $stmt->fetch(PDO::FETCH_ASSOC);

}

function addWishList($conn, $data=[])
{
    $stmt = $conn->prepare(
        "INSERT INTO favorite_item(fitem_code,evt_code) values (:fitem_code, :evt_code)"
    );
    $stmt = $conn->prepare($sql);
    $addNewWishitem = [
        'fitem_code' => $data['fitem_code'] ,
        'evt_code'   => $data['evt_code'], 
    ];

    return $stmt->execute($addNewWishitem);

}


function updateWishList($conn, $fitem_code, $evt_code)
{
    //$wish_list = json_encode($list,JSON_UNESCAPED_UNICODE);

    $stmt = $conn->prepare(
        "INSERT INTO `favorite_item` (`fitem_code`, `evt_code`) values (:fitem_code,:evt_code)"
    );

    return $stmt->execute([
        'fitem_code' => $fitem_code,
        'evt_code' => $evt_code
    ]);

}

function deleteWishList($conn, $fitem_code, $evt_code)
{
    $stmt = $conn->prepare(
        "INSERT INTO `favorites_item` ( `fitem_del_at`) values (:fitem_del_at)"
    );

    return $stmt->execute();

}



function findUserWishlist($conn, $csm_id)
{
    $stmt = $conn->prepare(
    "SELECT * FROM `favorite_item` WHERE `csm_id`= $csm_id");
    $stmt ->execute();
    $wishlist = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $userWishlist = [];
    foreach( $wishlist as $record )
    {
        $userWishlist[] = $record['fitem_code'];
    }

    return $userWishlist;
} 

function joinUserWishlist($conn, $userId)
{
    $stmt = $conn->prepare(
    "SELECT * FROM events e INNER JOIN favorite_item f ON e.evt_code=f.evt_code");
    $stmt ->execute();
}
/** < HW ====================================== >
/**
 * 依照給予的欄位與關鍵字，取得符合的展覽
 *
 * @param  PDO $conn       PDO實體
 * @param  string $search  要搜尋的關鍵字
 * @param  string $field   要依此搜尋關鍵字的欄位
 * @param  string $sort    要依此排序結果的欄位
 * @return object
 */
function findEventsLikeSearch($conn, $search, $field, $sort)
{
    //$sql = 搜尋events_category資料表中 $field 含有 $search 關鍵字的使用者 且依 $sort 欄位排序
    $sql = "SELECT * FROM `events_category` WHERE `{$field}` LIKE :search ORDER BY `{$sort}` ASC";

    //prepare($sql)
    //綁定:search 變數
    $stmt = $conn -> prepare($sql);
    $stmt -> bindValue(':search', "%".$search."%");
    $stmt -> execute();

    return $stmt->fetchAll(PDO::FETCH_CLASS, 'Users');//!Users還不知道要改成什麼
}//回傳 fetchAll(搜尋到的展覽資訊 以User物件型態)
/**
 * 更新使用者願望清單資料
 *
 * @param  PDO $conn     PDO實體
 * @param  string $id    要更新清單的展覽編號
 * @param  array $data   更新後的展覽清單資料
 * @return boolean       執行結果
 */
function findEventsList($conn, $evt_code, $evt_name)
{
    $stmt = $conn->prepare(
        "SELECT * FROM `eventslist` WHERE `evt_code`= $evt_code AND `evt_name`= $evt_name"
    );

    $stmt->execute();

    return $stmt->fetch(PDO::FETCH_ASSOC);

}
function addEventsList($conn, $evt_code, $evt_name)
{
    $stmt = $conn->prepare(
        "INSERT INTO eventslist(`evt_code`,`evt_name`) values ($evt_code, $evt_name)"
    );

    return $stmt->execute();

}
function updateEevntsList($conn, $evt_code, $evt_name)//!不確定是不是資料表中全部欄位都要打進去
{
    //$wish_list = json_encode($list,JSON_UNESCAPED_UNICODE);

    $stmt = $conn->prepare(
        "INSERT INTO `eventslist` (`evt_code`,`ven_cod`,`area_code`,`cat_code`,`evt_name	`,`evt_time`,`evt_price`,`evt_location`,`evt_describtion`,`evt_type`,`evt_viewtimes`) values (:evt_code,:ven_cod,:area_code,:cat_code,:evt_name,:evt_time,:evt_price,:evt_location,:evt_describtion,:evt_type,:evt_viewtimes)"
    );

    return $stmt->execute([
        'evt_code' => evt_code,
        'ven_cod' => ven_cod,
        'area_code' => area_code,
        'cat_code' => cat_code,
        'evt_name' => evt_name,
        'evt_time' => evt_time,
        'evt_price' => evt_price,
        'evt_location' => evt_location,
        'evt_describtion' => evt_describtion,
        'evt_type' => evt_type,
        'evt_viewtimes' => evt_viewtimes
    ]);

}
function deleteEventsList($conn, $evt_code, $evt_name)
{
    $stmt = $conn->prepare(
        "INSERT INTO `events` ( `evt_del_at`) values (:evt_del_at)"
    );

    return $stmt->execute();

}

/**
 * 購物車
 *
 * @param  PDO $conn     PDO實體
 * @param  string $id    要更新清單的展覽編號
 * @param  array $data   更新後的展覽清單資料
 * @return boolean       執行結果
 */

//取得所有cart
function fetchAllCart($conn)
{
    $stmt = $conn->prepare('SELECT * FROM cart_item');
    $stmt->execute();
    
    return $stmt->fetchAll(PDO::FETCH_ASSOC);  
}

//更新購物車
function updateCart($conn, $citem_code, $data = [])
{
    $sql="UPDATE `cart_item` SET citem_quntity=:quntity, citem_update_at=:update_date WHERE citem_code='{$citem_code}'";
    $stmt = $conn->prepare($sql);
    
    $updateCartitem = [
        'citem_quntity' => $data['quntity'],
        'citem_update_at' => $data['update_date']?? date('Y-m-d'),
    ];
    
    return $stmt->excute($updateCartitem);
        
        
}

//刪除購物車
function deleteCart($conn, $citem_code, $data=[])
{
    $sql="UPDATE `cart_item` SET citem_del_at=:delete_date WHERE citem_code='{$citem_code}'";
    $stmt = $conn->prepare($sql);
    
    $deleteCartitem = [
        'citem_del_at' => $data['delete_date'] ?? date('Y-m-d H:i:s'),
    ];
    
    return $stmt->excute($deleteCartitem);
}



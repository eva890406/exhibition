-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- 主機： localhost
-- 產生時間： 
-- 伺服器版本： 8.0.17
-- PHP 版本： 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `exhibition`
--

-- --------------------------------------------------------

--
-- 資料表結構 `cart_item`
--

CREATE TABLE `cart_item` (
  `id` int(11) NOT NULL,
  `events_id` int(11) NOT NULL,
  `consumers_member_id` int(11) NOT NULL,
  `quntity` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime NOT NULL,
  `delete_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `consumers_member`
--

CREATE TABLE `consumers_member` (
  `id` int(11) NOT NULL,
  `account` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `dob` date NOT NULL,
  `gender` varchar(45) NOT NULL,
  `age` int(11) NOT NULL,
  `create_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL,
  `delete_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 傾印資料表的資料 `consumers_member`
--

INSERT INTO `consumers_member` (`id`, `account`, `password`, `name`, `dob`, `gender`, `age`, `create_at`, `update_at`, `delete_at`) VALUES
(1, '1', '1', 'one', '2020-05-03', 'M', 22, NULL, NULL, NULL),
(2, '2', '2', 'two', '2020-05-13', 'F', 19, NULL, NULL, NULL),
(3, '3', '3', 'three', '2020-04-07', 'M', 30, NULL, NULL, NULL),
(4, 'en', '126456', 'en', '2000-11-27', '男', 20, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `vendors_member_id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  `events_category_id` int(11) NOT NULL,
  `name` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `eng_name` varchar(30) NOT NULL,
  `time` datetime NOT NULL,
  `price` int(11) NOT NULL,
  `location` varchar(45) NOT NULL,
  `describtion` text NOT NULL,
  `type` varchar(45) NOT NULL,
  `img` varchar(20) NOT NULL,
  `viewtimes` int(11) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime NOT NULL,
  `delete_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 傾印資料表的資料 `events`
--

INSERT INTO `events` (`id`, `vendors_member_id`, `area_id`, `events_category_id`, `name`, `eng_name`, `time`, `price`, `location`, `describtion`, `type`, `img`, `viewtimes`, `create_at`, `update_at`, `delete_at`) VALUES
(1, 2, 2, 1, '埃及法老展', '', '2020-06-01 12:00:00', 200, '220新北市板橋區民族路129巷10號', '法老是古埃及國王的尊稱，也是一個神秘的名字,習慣上把古埃及的國王通稱為法老。法老作為奴隸制專制君主，掌握全國的軍政、司法、宗教大權，其意志就是法律，是古埃及的最高統治者。法老自稱是太陽神阿蒙-賴神之子，是神在地上的代理人和化身。僅是法老的名字就具有不可抗拒的魔力，官員們以親吻法老的腳而感到自豪。\r\n\r\n原文網址：https://kknews.cc/history/625xxbl.html', 'culture', 'c1', 0, '2020-05-15 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 3, 1, 6, '恐龍展', '', '2020-06-09 10:00:00', 150, '220新北市板橋區民族路129巷10號', 'DINOSAUR!!!!', '生態', 'p1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 4, 8, 3, '陳澄波畫展', 'Chen Cheng Bo\'s Paintings', '2020-06-30 11:00:00', 400, '台中市西屯區台灣大道三段300號', '海上煙波──陳澄波藝術大展\r\nMisty Vapor on the High Seas – Chen Cheng-po\'s Art Exhibition\r\n\r\n\r\n1895年出生於臺灣嘉義的傑出畫家陳澄波，生逢清廷割臺的乙未之年；他以傑出的藝術表現和全力投入美術運動的精神，成為臺灣近代最具代表性的藝術家。陳澄波強烈的「祖國意識」，促使他在完成日本東京美術學校研究科的學業之後，便奔赴上海，先後擔任多所美術專科學校西畫科教授兼主任，並參與多項美術公職；曾獲選為「當代十二位代表畫家」之一，作品亦代表參展芝加哥世界博覽會。', '美術', 'a1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 4, 3, 3, '民初畫展', 'Early years of Republic of Chi', '2020-07-15 00:00:00', 100, '334桃園市八德區介壽路一段728號6樓', '十九世紀中期興起的金石書風，後亦擴及繪畫。最早由吳熙載開先河，後則有吳昌碩為大家，將北碑的用筆、篆刻的刀勢，融入繪畫之中，成為民初上海畫壇最具代表性的流派之一。吳昌碩本為浙江安吉人，三十歲初來到上海，受任頤指點，開始作畫，卓然成家。其畫風古拙蒼厚，化俗為雅，不僅風靡一時，更為當時的日本人所喜愛，具有跨國界的吸引力。吳昌碩的風格更影響及活動於上海的後輩，如王震，亦對日後在北京發展的齊白石、陳年有所啟發。本單元展出吳昌碩的作品，兼及其他與金石畫風相關的畫家作品，援以追索金石畫風自晚清至民國的發展。', '美術', 'a2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 2, 1, 3, '皇民化畫展', 'Imperialization', '2020-06-04 12:00:00', 120, '100台北市中正區八德路一段1號', '在日本殖民政策下，西洋畫、膠彩畫，透過國民教育體系成了當時台灣藝壇的主流。由於當時東渡日本或歐洲留學的台灣畫家，如陳澄波，凡在官辦的「台展」、「府展」中展露頭角者，大都成為台灣美術運動中的重要支持。他們組成畫會團體，對美術運動推波助瀾。加上參展的經濟誘因，也促本省早期畫家努力投入畫業。因而為台灣美術的近代化奠定了良好的基礎。', '美術', 'a3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 2, 1, 3, '現代畫展', 'Modern Paintings', '2020-08-13 10:00:00', 450, '111台北市士林區', '台灣50現代畫展－第五檔\r\n戰後50年以來 他們在這塊土地上為現代藝術奮鬥！\r\n\r\n\r\n展覽意義\r\n\r\n1.  現代繪畫系列的最後一檔，截自目前為止共邀請66位現代繪畫藝術家，展出經典及創新作品，為台灣時代脈動留下軌跡，盡現台灣現代藝術的多元面貌。\r\n\r\n2. 策展的邀請對象不一定是媒體寵兒，而是一群在台灣土地默默作畫創作的藝術家，他們堅持不輟，作品不僅出色且開創出自我風格，豐富了台灣現代藝術的廣度與厚度，讓台灣現代藝術大步向前邁進。\r\n\r\n3. 資深藝評家曾長生特別為系列展覽留下專業評論，肯定台灣50現代系列畫展的66位參展藝術家「自覺地面對自己以尋求創造性轉化，呈現了『跨文化現代性』，帶來『東方美學新結構』」！\r\n\r\n ', '美術', 'a4', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 1, 1, 3, '國際巨作畫展', 'International Masterpieces', '2020-06-30 11:00:00', 600, ' 111台北市士林區至善路二段221號', '文心藝術基金會繼上檔展覽 Danh Vo 的歷史敘事，本次文心藝所的展覽轉而探討現實的可能性。邀請觀眾走進阿麗莎·柯維德 Alicja Kwade的魔幻世界，重新質疑現存的觀念及想像多元的可能性。\r\n　　\r\n「為了尋求真相，我們有必要在生活中盡可能地懷疑所有事物」──法國哲學家笛卡兒\r\n　　\r\n幾千年來，人們一直試圖通過已知的方法及概念來度量、有限的詮釋現實(reality)。柯維德重新研究並質疑現實社會的結構，反思我們日常生活中對時空的感知。通過創造如同幻覺般的雕塑、裝置或影像，以數學、科學、哲學的方式重新闡述我們對時間、空間以及物質的概念。她的作品經常包括反射和重複的量體及聲音，使用這些元素來創造沉浸式和體驗式的空間，並鼓勵觀眾質疑他們對真實的信念。', 'Art', 'a5', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 1, 13, 3, '抽象畫展', 'Abstract Paintings', '2020-06-16 11:00:00', 360, '804高雄市鼓山區美術館路80號', '以抽象繪畫享譽國際的藝術家陳正雄，曾受李石樵、廖繼春、金潤作等前輩藝術家的指導，雖非美術創作傳統科班出身，卻仍秉持嚴謹的創作研究態度，開拓出屬於自己的藝術大道。他以不斷求新求變的作品，將他內在世界的真實，化為繽紛、豔麗、鮮活的畫面。他以資深藝術家恢宏的氣度，長期指導年輕的抽象藝術家，並積極奔走講授抽象藝術的理論，是台灣抽象藝術發展的重要推手。\r\n\r\n以藝術歌頌生命的喜悅\r\n\r\n陳正雄，1935年出生在台北一個商人之家，父親開明熱情，母親品味優雅。17歲時，經由父親好友畫家郭雪湖的介紹，進入著名寫實派畫家李石樵門下研習素描，及金潤作老師門下學習油畫。建中畢業後，他遵從父親的期待，進大學經濟系就讀。23 歲那年，他因閱讀俄國抽象藝術先驅康丁斯基經典名作《藝術的精神性》，受到啟發，從此專攻抽象藝術領域，持續以油彩、壓克力顏料或複合媒材創作，迄今已一甲子。陳正雄的作品畫面明朗、熱情且充滿活力。他將台灣原住民藝術獨特的色彩與生命力，及中國傳統書法狂草等東方元素，與西方現代藝術融合，呈現出屬於東方抒情的抽象繪畫。他以書寫線條融合豐富色彩，自由奔放的揮灑，形成澎湃迴盪，且充滿視覺意涵的律動空間。對此創作風格，他表示：「現代人的壓力大，生活緊張苦悶，我想畫的是讓大家看了都感到喜悅的畫，像是在畫布上畫出貝多芬《歡樂頌》音樂一樣，讓人感到放鬆。」\r\n\r\n1974年他獲選為英國皇家藝術學會終生院士，歷年來應邀參加巴黎著名的「五月沙龍展」，是獲邀參展的三位華裔畫家（另兩位為趙無極、朱德群）中唯一的台灣畫家。1999 年他以「窗」系列、2001 年以「數位空間」系列，兩度蟬連「義大利佛羅倫斯國際當代藝術雙年展」最高榮譽之「終生成就獎」及「偉大的羅倫佐」金質獎章，可謂是創下華人藝術家的新里程碑。他說：「畫家最重要特質，一是執著，堅守藝術立場，執著於創作理念，不變節、不妥協、始終如一；二是必須有專業精神，忠於藝術、忠於自己。」就是這股堅毅、不改初衷的意志，讓陳正雄自進入抽象藝術領域以來，能將東方的抒情抽象作為一生堅持繪畫創作的重要原因。他將此理念發揮到淋漓盡致，創作出一幅幅令人賞心悅目、回味無窮的作品。\r\n\r\n繼2004年創價首次邀請藝術家陳正雄舉辦「陳正雄巡迴展—顛覆與重建」，於全台五個藝文中心展出，獲得廣大迴響之後，此次2019年，創價與台東、宜蘭、花蓮縣政府合作，再次邀請陳正雄於東部盛大展出「東西融和‧開創新境─陳正雄抽象藝術展」。此次展覽囊括藝術家創作至今，各時期的轉折及歷程，誠摯邀請您一同來透過作品，感受藝術家內心奔放強大的力量，一起進入其豐富多彩的精神世界。', 'Art', 'a6', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- 資料表結構 `events_area`
--

CREATE TABLE `events_area` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 傾印資料表的資料 `events_area`
--

INSERT INTO `events_area` (`id`, `name`) VALUES
(1, '台北市'),
(2, '新北市'),
(3, '桃園市'),
(4, '新竹縣市'),
(5, '基隆縣市'),
(6, '宜蘭縣市'),
(7, '苗栗縣市'),
(8, '台中市'),
(9, '彰化縣市'),
(10, '雲林縣市'),
(11, '嘉義縣市'),
(12, '台南市'),
(13, '高雄市'),
(14, '屏東縣市'),
(15, '花蓮縣市'),
(16, '台東縣市'),
(17, '澎湖縣市'),
(18, '金馬縣市');

-- --------------------------------------------------------

--
-- 資料表結構 `events_category`
--

CREATE TABLE `events_category` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 傾印資料表的資料 `events_category`
--

INSERT INTO `events_category` (`id`, `name`) VALUES
(1, 'Culture'),
(2, 'Foodstuff'),
(3, 'Art'),
(4, 'Science and Technology'),
(5, 'Design'),
(6, 'Ecology');

-- --------------------------------------------------------

--
-- 資料表結構 `favorite_item`
--

CREATE TABLE `favorite_item` (
  `id` int(11) NOT NULL,
  `events_id` int(11) NOT NULL,
  `consumers_member_id` int(11) NOT NULL,
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` datetime DEFAULT NULL,
  `delete_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 傾印資料表的資料 `favorite_item`
--

INSERT INTO `favorite_item` (`id`, `events_id`, `consumers_member_id`, `create_at`, `update_at`, `delete_at`) VALUES
(1, 1, 2, '2020-06-01 15:17:14', NULL, '2020-06-01 08:14:07'),
(2, 1, 1, '2020-06-01 15:17:14', NULL, '2020-06-02 06:56:41'),
(3, 2, 1, '2020-06-01 15:22:32', NULL, NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `payment` int(11) NOT NULL,
  `status` varchar(45) NOT NULL,
  `paydate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `orders_item`
--

CREATE TABLE `orders_item` (
  `id` int(11) NOT NULL,
  `orders_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `create_at` datetime NOT NULL,
  `delete_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `system_manager`
--

CREATE TABLE `system_manager` (
  `id` int(11) NOT NULL,
  `account` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `gender` varchar(45) NOT NULL,
  `create_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL,
  `delete_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 傾印資料表的資料 `system_manager`
--

INSERT INTO `system_manager` (`id`, `account`, `password`, `name`, `gender`, `create_at`, `update_at`, `delete_at`) VALUES
(407402100, '407402100', '407402100', 'Eva', 'F', NULL, NULL, NULL),
(407402564, '407402564', '407402564', '董家含', 'F', NULL, NULL, NULL),
(407402576, '407402576', '407402576', 'Claudia', 'F', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `tasks`
--

CREATE TABLE `tasks` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `events_id` int(11) NOT NULL,
  `system_manager_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `vendors_member`
--

CREATE TABLE `vendors_member` (
  `id` int(11) NOT NULL,
  `account` varchar(45) NOT NULL,
  `password` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `intro` text NOT NULL,
  `contact` text NOT NULL,
  `create_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL,
  `delete_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 傾印資料表的資料 `vendors_member`
--

INSERT INTO `vendors_member` (`id`, `account`, `password`, `name`, `intro`, `contact`, `create_at`, `update_at`, `delete_at`) VALUES
(1, '12345', 12345, '朱銘美術館', '朱銘美術館位於新北市金山區，在1999年9月成立，依山傍水，館區開放8甲；其實最初朱銘先生是為了解決大型作品的存放問題，後來藝術家的性情讓他從大自然中找到靈感，他決定興建一處展現他畢生傑作，以及供藝術家發揮創意的園地；朱銘先生針對不同的地形地貌，朱銘美術館園區規劃有服務中心、第一展覽室、會議室、藝術表演區、藝術交流區、朱雋區、戲水區、太極廣場、美術館本館、人間廣場、慈母碑、天鵝池、藝術長廊、運動廣場。', '聯絡人:胡馨月\r\n聯絡信箱:hsinyueh.hu@juming.org.tw\r\n聯絡電話:(02)-24989940(分機:1515) 胡馨月', NULL, NULL, NULL),
(2, '12346', 12346, '駁二藝術特區', '在駁二這個地方，衝突是一股美好的力量。\r\n被歷史塵封的陳舊倉庫，時光凝結了種種發展跡象，因為藝術文化的呼聲被解放，不斷注入創意靈感而重獲新生，日漸茁壯。在舊與新的衝突中，衝撞出的是勃發的生命力。\r\n\r\n位於碼頭旁的倉庫群藝術特區，出出入入的除了車輛還有船舶，湊熱鬧的不只是人群還有海風，開闊的海天視野一如遼闊無垠的思緒。在海陸交界的衝突裡，寬廣的是耀眼的未來。\r\n\r\n負責營運管理的公部門與天馬行空的藝術家，雖然各自擁抱夢想，但在這個場域裡相互協調步伐，修整方向，一起邁向共同的理想。在行政與藝術的衝突間，磨合成緊密的夥伴，建構駁二在每個人心中的形象。\r\n\r\n美好的力量，需要時間蘊釀與心力累積。\r\n2000年，雙十煙火第一次不侷限在台北施放，決定南下高雄綻放，為尋覓適當的放煙火地點，人們發現了港口旁駁二倉庫的存在，一群熱心熱血的藝文界人士於2001年成立駁二藝術發展協會，催生推動駁二藝術特區作為南部人文藝術發展的基地。\r\n\r\n2006年，高雄市政府文化局接手駁二藝術特區，高雄設計節、好漢玩字節、鋼雕藝術節、貨櫃藝術節、高雄人來了大公仔、Live Warehouse駁二音樂演唱會，每一個充滿城市創意特質的展演，活力豐沛的在駁二不斷呈現嶄新的概念與樣貌，構築海港城市的魅力文化與生活美學。\r\n\r\n累積與醞釀，將適時的迸發。\r\n無論過去或現在，駁二藝術特區都是高雄人不可缺少的倉庫群。過去儲放魚粉與砂糖，供應港口川流的繁華歲月，現在匯集設計與創意能量，豐富每一個自由想像的靈魂。過去港邊載送貨物的西臨港線鐵道，現在成為高雄最熱門的水岸輕軌，貫穿駁二藝術特區，而駁二的倉庫群跳脫使用僵制，以藝文與世界接軌，開啟新世代的對話。\r\n\r\n特殊的環境位置在全台的藝文場域中獨樹一格，駁二這個極具特色的藝文空間，同時也是民眾最佳休閒場域，踩著自行車可以遨遊西臨港線的海景風光，公共空間裡可以發現令人驚喜的藝術作品，映著藍天豔陽，隨處都是拍照的絕佳場景，搭配不同時節精心推出的特色展演，讓民眾的每次到訪都能擁有不同驚豔！', '地址：高雄市鹽埕區大勇路1號\r\n聯絡信箱:piertwoart@gmail.com\r\n聯絡電話:(07)-5214899', NULL, NULL, NULL),
(3, '12347', 12347, '陽明海洋文化藝術館', '「陽明海洋文化藝術館」原是一棟竣工於1915年5月4日為日本郵船株式會社所有，係當時流行之「歷史樣式」建築，是日據時期基隆西岸碼頭重要建築，對於基隆航運地位具有特殊之指標意義。\r\n\r\n台灣光復後由招商局接管迄陽明海運公司經營至今。經歷過二次世界大戰的烽火和歲月的洗禮，目前僅能從外牆立面以及復古典雅的圓弧型拱廊，大致看出日治時代的舊貌。\r\n\r\n\r\n陽明海運公司基於對海洋文化傳承的高度關懷，對歷史建築「活化」的再利用，於2003年起著手規劃整建，在保有建築物本身古樸的風貌之餘，更以海洋文化為經，人文藝術為緯，灌注出新生命、新精神、新氣象、新願景，\r\n\r\n故命名為「陽明海洋文化藝術館」，並於2004年12月28日修繕竣工開館啟用，期能提供民眾一處與海洋文化邂逅的場所，以心領略海洋與人文、歷史、地理、藝術、工藝等人類文明發展多元化的面貌，使觀眾洋洋乎與天地同遊而不知其窮。', '地址：基隆市仁愛區港西街4號(基隆火車站前)\r\n電話：(02)2421-5681\r\n營業時間：週二至週日上午9時至下午5時( 門票最後販售時間：下午4:30 )\r\n休 館：每週一、除夕、春節初 一', NULL, NULL, NULL),
(4, '12348', 12348, '羊毛氈手創館', '一個 擁護手作 | 崇尚自然 | 傳遞溫暖 的羊毛氈品牌\r\n在這裡 每一件商品都是獨一無二的作品\r\n純粹的手工 讓每個產出的當下都無法複製\r\n猶如你我在這世界的獨特性一般 	  	 \r\n	\r\n・藝術離不開手作\r\n任何一種藝術創作都源自手作；\r\n手工消逝了，藝術也不復存在。\r\n我們相信：\r\n手作可以找回生活的本質，\r\n那是一種純粹地令人感動的質感。\r\n \r\n\r\n・自然永不退流行\r\n以純天然的羊毛當素材、\r\n流傳千年的古老技藝為手法、\r\n友善環境的生產方式作原則，\r\n讓生活回歸自然的狀態，\r\n創造流行的恆久性。\r\n \r\n	\r\n・用消費傳遞溫暖\r\n消費是一種形塑自我風格的過程，更是價值觀的展現。\r\n透過跨國的設計合作，協助第三世界國家保存傳統技藝、\r\n提高手工價值，脫離貧窮與失學的困境。\r\n藉由消費思維的轉換，\r\n你也可以溫暖地球另一端的家庭！\r\n\r\n	 \r\n・行動主張 \r\n我們以取之於自然的羊毛，用之於生活的手作出發， 不同於工業的製程，減少資源的浪費與廢棄物的產生。羊毛是自然生長的原料，可以與自然環境進行完全的分解。手作也少了機具的使用，大大降低噪音與空汙與對環境的影響，以這樣無汙染、無多餘廢棄物的材質對生態的永續盡一份心力。\r\n\r\n \r\n減少不必要的包裝一直是我們的目標之一。我們適量的使用塑膠，以材質可回收的夾鏈袋，讓重複使用與減少購買實現，另以100%舊報紙回收製成的雞蛋盒為另一種包裝，使用完內容物後，盒子還可以當作其他物件的收納，降低對樹木的使用與需求。\r\n\r\n\r\n \r\n\r\n自古我們與動物共同生活著，為了方便取得生活所需，慢慢發展成眷養的方式，在轉變的過程中，我們同時關注動物權利的平等關係。我們的羊毛原料均來自紐澳以人道飼養及人道取得的農場，通過嚴格的認證，確保每一隻羊在取毛的過程中，沒有受到虐待的可能，取之於自然的同時，我們也關心取得的過程是否溫柔。', '營業時間:週二-週六 13:00-21:00\r\n門市地址:台北市106新生南路三段60巷9號\r\n客服電話:02-3365-3302', NULL, NULL, NULL),
(7, 'dtuyuk', 0, '1234567', '*請輸入主辦方簡介', '*請輸入聯絡方式(聯絡人/電話/E-mail等)', NULL, NULL, NULL);

--
-- 已傾印資料表的索引
--

--
-- 資料表索引 `cart_item`
--
ALTER TABLE `cart_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `evt_code` (`events_id`,`consumers_member_id`),
  ADD KEY `cart_item_ibfk_1` (`consumers_member_id`);

--
-- 資料表索引 `consumers_member`
--
ALTER TABLE `consumers_member`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ven_code` (`vendors_member_id`,`area_id`,`events_category_id`),
  ADD KEY `events_ibfk_1` (`area_id`),
  ADD KEY `events_ibfk_3` (`events_category_id`);

--
-- 資料表索引 `events_area`
--
ALTER TABLE `events_area`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `events_category`
--
ALTER TABLE `events_category`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `favorite_item`
--
ALTER TABLE `favorite_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `evt_code` (`events_id`),
  ADD KEY `csm_id` (`consumers_member_id`);

--
-- 資料表索引 `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `orders_item`
--
ALTER TABLE `orders_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_code` (`orders_id`);

--
-- 資料表索引 `system_manager`
--
ALTER TABLE `system_manager`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `evt_code` (`events_id`,`system_manager_id`),
  ADD KEY `tasks_ibfk_2` (`system_manager_id`);

--
-- 資料表索引 `vendors_member`
--
ALTER TABLE `vendors_member`
  ADD PRIMARY KEY (`id`);

--
-- 在傾印的資料表使用自動遞增(AUTO_INCREMENT)
--

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `cart_item`
--
ALTER TABLE `cart_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `consumers_member`
--
ALTER TABLE `consumers_member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `events_area`
--
ALTER TABLE `events_area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `events_category`
--
ALTER TABLE `events_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `favorite_item`
--
ALTER TABLE `favorite_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `system_manager`
--
ALTER TABLE `system_manager`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=407402577;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `vendors_member`
--
ALTER TABLE `vendors_member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- 已傾印資料表的限制式
--

--
-- 資料表的限制式 `cart_item`
--
ALTER TABLE `cart_item`
  ADD CONSTRAINT `cart_item_ibfk_1` FOREIGN KEY (`consumers_member_id`) REFERENCES `consumers_member` (`id`);

--
-- 資料表的限制式 `events`
--
ALTER TABLE `events`
  ADD CONSTRAINT `events_ibfk_1` FOREIGN KEY (`area_id`) REFERENCES `events_area` (`id`),
  ADD CONSTRAINT `events_ibfk_2` FOREIGN KEY (`vendors_member_id`) REFERENCES `vendors_member` (`id`),
  ADD CONSTRAINT `events_ibfk_3` FOREIGN KEY (`events_category_id`) REFERENCES `events_category` (`id`);

--
-- 資料表的限制式 `favorite_item`
--
ALTER TABLE `favorite_item`
  ADD CONSTRAINT `favorite_item_ibfk_1` FOREIGN KEY (`events_id`) REFERENCES `events` (`id`),
  ADD CONSTRAINT `favorite_item_ibfk_2` FOREIGN KEY (`consumers_member_id`) REFERENCES `consumers_member` (`id`);

--
-- 資料表的限制式 `orders_item`
--
ALTER TABLE `orders_item`
  ADD CONSTRAINT `orders_item_ibfk_1` FOREIGN KEY (`orders_id`) REFERENCES `orders` (`id`);

--
-- 資料表的限制式 `tasks`
--
ALTER TABLE `tasks`
  ADD CONSTRAINT `tasks_ibfk_1` FOREIGN KEY (`events_id`) REFERENCES `events` (`id`),
  ADD CONSTRAINT `tasks_ibfk_2` FOREIGN KEY (`system_manager_id`) REFERENCES `system_manager` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

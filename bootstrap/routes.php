<?php
declare(strict_types=1);

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;

return function (App $app) {

    $app->get('/', function (Request $request, Response $response, $args) {

        render('index', [
            'msg' => 'hello',
        ]);

        return $response;
    });
    /* =========================================================================
    * = LOGIN
    * =========================================================================
    */
    $app->get('/cus_login', function (Request $request, Response $response, $args) {

        render('cus_login', [
            'msg' => 'hello',
        ]);

        return $response;
    });

    $app->get('/ven_login', function (Request $request, Response $response, $args) {

        render('ven_login', [
            'msg' => 'hello',
        ]);

        return $response;
    });

    $app->post('/consumerLogin', function (Request $request, Response $response, $args) {

        $data = $request->getParsedBody(); //$_POST

        $account = $data['account'] ?? '';
        $password = $data['password'] ?? '';


        $result = verifyConsumerLogin($account, $password);
        if ($result) {
            $cusName = DB::find('consumers_member', $account, 'account');
            $_SESSION["cusId"] = $cusName['id'];
            render('index', ['msg' => 'Hello','cus_id' => $_SESSION["cusId"],'name' => $cusName['name'],]);
        } else {
            render('cus_login', ['msg' => '帳號密碼錯誤',]);
        }

        return $response;
    });

    $app->post('/vendorLogin', function (Request $request, Response $response, $args) {

        $data = $request->getParsedBody(); //$_POST

        $account = $data['account'] ?? '';
        $password = $data['password'] ?? '';

        $result = verifyVendorLogin($account, $password);
        if ($result) {
            $venName = DB::find('vendors_member', $account, 'account');
            $_SESSION["venId"] = $venName['id'];
            render('index', ['msg' => 'HELLO','ven_id' => $_SESSION["venId"],'name' => $venName['name'],]);
        } else {
            render('ven_login', ['msg' => '帳號密碼錯誤',]);
        }

        return $response;
    });

    $app->post('/managerLogin', function (Request $request, Response $response, $args) {

        $data = $request->getParsedBody(); //$_POST

        $account = $data['account'] ?? '';
        $password = $data['password'] ?? '';

        $result = verifyManagerLogin($account, $password);
        if ($result) {
            render('index', ['msg' => $account,]);
        } else {
            render('index', ['msg' => '帳號密碼錯誤',]);
        }

        return $response;
    });

    /* =========================================================================
    * = SIGN UP MEMBER
    * =========================================================================
    */

    $app->get('/cus_signup', function (Request $request, Response $response, $args) {

        render('cus_signup');

        return $response;

    });
    
    $app->post('/signupConsumersMember', function (Request $request, Response $response, $args) {
 
        $data = $request->getParsedBody();

        $result = DB::create('consumers_member', $data);

        render('cus_login', ['msg' => $result ? '註冊成功':'註冊失敗']);

        return $response;

    });

    $app->get('/ven_signup', function (Request $request, Response $response, $args) {

        render('ven_signup');

        return $response;

    });

    $app->post('/signupVendorsMember', function (Request $request, Response $response, $args) {
 
        $data = $request->getParsedBody();
        //var_dump($data);die;
        $result = DB::create('vendors_member', $data);

        render('ven_login', ['msg' => $result ? '註冊成功':'註冊失敗']);

        return $response;

    });

    $app->post('/signupSystemManager', function (Request $request, Response $response, $args) {
 
        $data = $request->getParsedBody();

        $result = DB::create('system_managers', $data);

        render('mag_login', ['msg' => $result ? '註冊成功':'註冊失敗']);

    });

    /* =========================================================================
    * = events
    * =========================================================================
    */




    $app->get('/ex_add', function (Request $request, Response $response, $args) {

        render('ex_add');

        return $response;

    });

    $app->post('/events/add', function(Request $request, Response $response, $args){

        $data = $request->getParsedBody();
        //var_dump($data);die;

        $result = DB::create('events', $data);

        render('ex_add', ['msg' => $result ? '新增成功' : '新增失敗']);

        return $response;
    });

    $app->get('/ex_modify', function (Request $request, Response $response, $args) {

        render('ex_modify');

        return $response;

    });

    $app->post('/events/update', function(Request $request, Response $response, $args){

        $data = $request->getParsedBody();

        $id = $data['id'] ?? '';

        $result = DB::update('events', "`id` = {$id}", $data);

        render('', ['msg' => $result ? '修改成功' : '修改失敗']);

        return $response;

    });

    $app->post('/events/delete', function(Request $request, Response $response, $args){

        $data = $request->getParsedBody();

        $id = $data['id'] ?? '';

        $delete_at = $data['delete_at'] ?? date("Y-m-d H:i:s");

        $result = DB::update('event', "`id` = {$id}", $delete_at);

        return $response;
    });

    /* =========================================================================
    * = CATEGORY
    * ==========================================================================
    */

    $app->get('/category', function (Request $request, Response $response, $args) {

        render('category');

        return $response;

    });

    $app->get('/artlist', function(Request $request, Response $response, $args){

        $category = DB::find('events_category', 'Art', 'name');
        $art = DB::findAll('events',$category['id'] ,'events_category_id', 'delete_at');


        render('artlist', ['event_art' => $art]);

        return $response;
    });

    $app->get('/artlist/{id}', function(Request $request, Response $response, $args){

        $id = $args['id'];
        $event = DB::find('events', $id, 'id');

        render('artlist', ['event' => $event]);

        return $response;
    });



    $app->get('/culture/{id}', function(Request $request, Response $response, $args){

        $id = $args['id'];
        $event = DB::find('events', $id, 'id');

        render('culture', ['event' => $event]);
        return $response;

    });   

    $app->get('/designlist', function (Request $request, Response $response, $args) {

        render('designlist');

        return $response;

    });

    $app->get('/designlist/{id}', function(Request $request, Response $response, $args){

        $id = $args['id'];
        $event = DB::find('events', $id, 'id');

        render('designlist', ['event' => $event]);

        return $response;
    });

    $app->get('/foodlist', function (Request $request, Response $response, $args) {

        render('foodlist');

        return $response;

    });

    $app->get('/sciencelist', function (Request $request, Response $response, $args) {

        render('sciencelist');

        return $response;

    });

    $app->get('/ecolist', function (Request $request, Response $response, $args) {

        render('ecolist');

        return $response;
    });

    $app->get('/fetchAllEvents', function(Request $request, Response $response, $args){

        $events = DB::fetchAll('events');
    
        var_dump($events['name']);

        return $response;
        
    });

    $app->get('/culturelist', function (Request $request, Response $response, $args) {

        $name = 'culture';

        $result = DB::find('events_category', $name, 'name');
        //var_dump($result);die;
        $event = DB::findAll('events', $result['id'], 'events_category_id','delete_at');
        
        render('culturelist', ['events' => $event]);

        return $response;
    });

    /* =========================================================================
    * = AREA
    * ==========================================================================
    */

    $app->get('/area', function(Request $request, Response $response, $args){

        $area = DB::fetchAll('events_area');
        var_dump($area['name']);

        return $response;

    });

    $app->get('/area/search/{id}', function(Request $request, Response $response, $args){

        $areaId = $args['id'];

        $area = DB::find('events_area', $areaId, 'id');
        $areaEventsId = $area['events_id'];
        $events = DB::find('events', $areaEventsId, 'id');

        var_dump($events['name']);

        return $response;

    });

    /* =========================================================================
    * = BUCKETLIST
    * ==========================================================================
    */

    $app->get('/bucketlist', function(Request $request, Response $response, $args){
        session_start();
        $cusId = $_SESSION["cusId"]??'1';
        $buckets = DB::findAll('favorite_item', $cusId, 'consumers_member_id', 'delete_at');
        $events = bucketlist_Checkoutdelete($buckets);
        $single = singlebucket($buckets);

       // var_dump($events);die;
        render('bucketlist', ['events' => $events,'buckets' => $single,
            ]);

        return $response;

    });

    $app->post('/bucketlist_delete/{id}', function(Request $request, Response $response, $args){
        session_start();
        $cusId = $_SESSION["cusId"];
        $id = $args['id'];
        //var_dump($id);die;
        $delete = ['delete_at' => date("Y-m-d H:i:s")];
        $result = DB::update('favorite_item', "`id` = {$id}", $delete);
        
        //delete 更新資料回到bucketlist
        $buckets = DB::findAll('favorite_item', $cusId, 'consumers_member_id', 'delete_at');
        $events = bucketlist_Checkoutdelete($buckets);
        $single = singlebucket($buckets);

        render('bucketlist', ['events' => $events,'buckets' => $single,
            ]);
      
        return $response;
    });

    $app->post('/bucketlist_cart/{id}', function(Request $request, Response $response, $args){

        $id = $args['id'];
        $fitem = DB::find('favorite_item', $id, 'id');
        $eventitem = DB::find('events', $fitem['events_id'], 'id');
        $result = DB::update('favorite_item', "`id` = {$id}", $delete);
        
        //delete 更新資料回到bucketlist
        session_start();
        $cusId = $_SESSION["cusId"];
        $buckets = DB::findAll('favorite_item', $cusId, 'consumers_member_id', 'delete_at');
        $events = bucketlist_Checkoutdelete($buckets);

        render('bucketlist', ['events' => $events,
            ]);
      
        return $response;
    });

    
    /* =========================================================================
    * = CART
    * ==========================================================================
    */

    $app->get('/cart', function(Request $request, Response $response, $args){
        session_start();
        $cusId = $_SESSION["cusId"]??'1';
        $cart = DB::findAll('cart_item', $cusId, 'consumers_member_id', 'delete_at');
        $events = cart_checkout($cart);
       // var_dump($events);die;
        render('cart', ['events' => $events,
            ]);

        return $response;

    });

    $app->post('/cart_newItem/{id}', function(Request $request, Response $response, $args){
        $data = $request->getParsedBody();

        $id = $args['id'];
        $event = DB::find('events', $id, 'id');
        $result = DB::create('cart_item', $data);
        $delete = $data['delete_at'] ?? date("Y-m-d H:i:s");
        $deletebucket = DB::update('favorite_item', "`events_id` = {$id}", $delete);

        render('cart',);

        return $response;

    });
    /* =========================================================================
    * = UPDATE MEMBER
    * =========================================================================
    */

    $app->post('/UpdateConsumersMember', function (Request $request, Response $response, $args) {
 
        $data = $request->getParsedBody();

        $id = $data['id'] ?? '';
        

        $result = DB::update('consumers_member',"`id` = {$id}" ,$data);

        render('signupupdate', ['msg' => $result ? '修改成功':'修改失敗']);

    });

    $app->post('/UpdateVendorsMember', function (Request $request, Response $response, $args) {
 
        $data = $request->getParsedBody();

        $id = $data['id'] ?? '';

        $result = DB::update('vendors_member',"`id` = {$id}", $data);

        render('signupupdate', ['msg' => $result ? '修改成功':'修改失敗']);

    });

    $app->post('/UpdateSystemManager', function (Request $request, Response $response, $args) {
 
        $data = $request->getParsedBody();

        $id = $data['id'] ?? '';

        $result = DB::update('system_managers',"`id` = {$id}", $data);

        render('signupupdate', ['msg' => $result ? '修改成功':'修改失敗']);

    });
    /* =========================================================================
    * = DELETE MEMBER
        使用軟刪除新增刪除時間
    * =========================================================================
    */

    $app->post('/DeleteConsumersMember', function (Request $request, Response $response, $args) {
 
        $data = $request->getParsedBody();

        $id = $data['id'] ?? '';
        $delete_at = $data['delete_at'] ?? date(Y-m-d);
        

        $result = DB::update('consumers_member',"`id` = {$id}", $data);

        render('signupdelete', ['msg' => $result ? '刪除成功':'刪除失敗']);

    });

    $app->post('/DeleteVendorsMember', function (Request $request, Response $response, $args) {
 
        $data = $request->getParsedBody();

        $id = $data['id'] ?? '';
        $delete_at = $data['delete_at'] ?? date(Y-m-d);

        $result = DB::update('vendors_member',"`id` = {$id}", $data);

        render('signupdelete', ['msg' => $result ? '刪除成功':'刪除失敗']);

    });

    $app->post('/DeleteSystemManager', function (Request $request, Response $response, $args) {
 
        $data = $request->getParsedBody();

        $id = $data['id'] ?? '';
        $delete_at = $data['delete_at'] ?? date(Y-m-d);

        $result = DB::update('system_managers',"`id` = {$id}", $data);

        render('signupdelete', ['msg' => $result ? '刪除成功':'刪除失敗']);

    });
/* =========================================================================
    * = FIND EXHIBITION 
    從events_category找name的id，去對應events裡面的events_category_id
    * =========================================================================
    **/

    //Culture
    $app->get('/FindExibitionCulture', function (Request $request, Response $response, $args) {

        $name = 'Culture';

        $result = DB::find('events_category', $name, 'name');
        

        return $response;
    });

    //Foodstuff
    $app->get('/FindExibitionFoodstuff', function (Request $request, Response $response, $args) {

        
        $name = 'Foodstuff';

        $result = DB::find('events_category', $name, 'name');
        

        return $response;
    });

    //Art
    $app->get('/FindExibitionArt', function (Request $request, Response $response, $args) {

        $name = 'Art';

        $result = DB::find('events_category', $name, 'name');
        

        return $response;
    });

    //Science and Technology
    $app->get('/FindExibitionSandT', function (Request $request, Response $response, $args) {

        $name = 'SandT';

        $result = DB::find('events_category', $name, 'name');
        

        return $response;
    });

    //Design
    $app->get('/FindExibitionDesign', function (Request $request, Response $response, $args) {

        $name = 'Design';

        $result = DB::find('events_category', $name, 'name');
        

        return $response;
    });

    //Ecology
    $app->get('/FindExibitionEcology', function (Request $request, Response $response, $args) {

        $name = 'Ecology';

        $result = DB::find('events_category', $name, 'name');
        

        return $response;
    });
    /* =========================================================================
    * = DRIVER
    * =========================================================================
    **/
    $app->get('/driver', function (Request $request, Response $response, $args){

        var_dump(DB::fetchAll('driver'));

        return $response;
    });

    $app->get('/driver/{id}', function (Request $request, Response $response, $args) {

        $driverId = $args['id'];

        //司機駕駛的公車
        $bus = DB::find('bus', $driverId, 'driver_id');
        $departTime = $bus['DEPART_TIME'];

        var_dump(countTimeToArriveNextStop($departTime));

        return $response; 
    });

    /* =========================================================================
    * = STOP
    * =========================================================================
    **/
    $app->get('/stop', function (Request $request, Response $response, $args) {

        render('stop', ['msg' => '增加站牌資訊',]);

        return $response;
    });

    $app->post('/stop/add', function (Request $request, Response $response, $args) {

        $data = $request->getParsedBody();

        $result = DB::create('stop', $data);
        //['id'=>'1','name'=>'tiny']

        render('stop', ['msg' => $result ? '增加站牌資訊成功':'增加站牌資訊失敗',]);

        return $response;
    });

    $app->post('/stop/update', function (Request $request, Response $response, $args) {

        $data = $request->getParsedBody();

        $stopId = $data['STOP_ID'];

        $result = DB::update('stop', "`STOP_ID` = {$stopId}", $data);
    });

};
